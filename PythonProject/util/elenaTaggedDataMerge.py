import csv
import re
import codecs

IN_NEW_CORPUS = '../../Data/BhatiaDev/bhatia_elena_tagging_merge/bhatia_untagged_new_export.tsv'
IN_OLD_TAGGED_CORPUS = '../../Data/BhatiaDev/bhatia_elena_tagging_merge/bhatia_elena_tagged_old_export.tsv'
OUT_CORPUS = '../../Data/BhatiaDev/bhatia_elena_tagging_merge/bhatia_merged_corpus.csv'

# read both corpuses

if '\0' in open(IN_NEW_CORPUS).read():
    print("you have null bytes in your input file")
else:
    print("you don't")

untagged_examples = []
with open(IN_NEW_CORPUS, 'r') as f:
    reader = csv.DictReader(f, delimiter='\t')
    for example in reader:
        untagged_examples.append(example)

tagged_examples = []
with open(IN_OLD_TAGGED_CORPUS, 'r') as f:
    reader = csv.DictReader(f, delimiter='\t')
    for example in reader:
        tagged_examples.append(example)


# do the merge
merged_examples = []
tagged_example_idx = 0
for untagged_example in untagged_examples:
    if tagged_example_idx < len(tagged_examples) and re.sub("^\+*", "", untagged_example["sentence"]) == re.sub("^\+*", "", tagged_examples[tagged_example_idx]["sentence"]):
        untagged_example["class1"] = tagged_examples[tagged_example_idx]["class1"]
        untagged_example["class2"] = tagged_examples[tagged_example_idx]["class2"]
        untagged_example["class3"] = tagged_examples[tagged_example_idx]["class3"]
        tagged_example_idx += 1
    merged_examples.append(untagged_example)


# write merged corpus
with open(OUT_CORPUS, 'w') as csvfile:
    fieldnames = ['class1', 'class2', 'class3', '0', 'sentence', 'id']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter='\t')

    writer.writeheader()
    for example in merged_examples:
        writer.writerow(example)