from util.datasetUtil import test_split, importDataStandard
from util.bhatiaImport import importBhatiaDataset

#examples = importDataStandard()
examples = importBhatiaDataset()

(X_train, y_train), (X_test, y_test) = test_split(examples, 'sentence', 'class_1', 0.3)

with open('../../Data/corpus_fastText_format_train.txt', 'w') as f:
    for sentence, label in zip(X_train, y_train):
        f.write(sentence + ' __label__' + str(label) + '\n')

with open('../../Data/corpus_fastText_format_test.txt', 'w') as f:
    for sentence, label in zip(X_test, y_test):
        f.write(sentence + ' __label__' + str(label) + '\n')

with open('../../Data/corpus_fastText_format_test_nolabel.txt', 'w') as f:
    for sentence, label in zip(X_test, y_test):
        f.write(sentence + '\n')