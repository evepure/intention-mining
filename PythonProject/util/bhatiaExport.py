from bs4 import BeautifulSoup
import glob
from constants import BHATIA_LOCATION
import nltk.data
import csv


# See importantDatasetStandard in util.datasetUtil for more info


sent_detector = nltk.data.load('tokenizers/punkt/english.pickle')


def exportBhatiaDatasetToCsv(filename):
    examples = []

    for file in glob.glob(BHATIA_LOCATION):
        id = file[file.rfind('/') + 1:len(file) - 4]
        post_index = 0

        soup = BeautifulSoup(open(file, encoding="mac_roman"), 'html.parser')

        sent_index = 0
        for sentence in sent_detector.tokenize( soup.thread.initpost.icontent.string.strip() ):
            example = {}
            example['sentence'] = sentence
            example['id'] = id + '_' + str(sent_index)
            examples.append(example)
            sent_index += 1
        post_index += 1

        for post in soup.find_all('post'):
            for sentence in sent_detector.tokenize( post.rcontent.string.strip() ):
                example = {}
                example['sentence'] = ''.join(['+' for s in range(post_index)]) + sentence
                example['id'] = id + '_' + str(sent_index)
                examples.append(example)
                sent_index += 1
            post_index += 1

    with open(filename, 'w') as csvfile:
        fieldnames = ['id', 'sentence']
        writer = csv.DictWriter(csvfile, fieldnames=fieldnames, delimiter = '\t')

        writer.writeheader()
        for example in examples:
            writer.writerow(example)


exportBhatiaDatasetToCsv("../../Data/BhatiaDev/bhatia_untagged.csv")