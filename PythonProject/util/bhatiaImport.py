from bs4 import BeautifulSoup
import glob
from constants import BHATIA_LOCATION
import nltk


# See importantDatasetStandard in util.datasetUtil for more info
from preprocessing import preprocessing, dataCleaning


# Int to value mapping
# 1 = Question
# 2 = Repeat question
# 3 = Clarification
# 4 = Further details
# 5 = Solution
# 6 = Negative feedback
# 7 = Positive feedback
# 8 = Junk
# -1 = Other

bhatiaToOurTag = {"1": "DIRECT", "3": "DIRECT", "4": "ASSERT", "5": "ASSERT", "7": "REJOICE", "6": "COMPLAIN", "2": "TO_OMIT", "8": "TO_OMIT", "-1": "TO_OMIT"}

def importBhatiaDataset():
    examples = []


    for file in glob.glob(BHATIA_LOCATION):
        id = file[file.rfind('/')+1:len(file)-4]
        index = 0

        soup = BeautifulSoup(open(file, encoding="mac_roman"), 'html.parser')

        example = {}
        example['class_1'] = bhatiaToOurTag[soup.find_all('class')[0].string.strip()]
        example['sentence'] = soup.thread.initpost.icontent.string.strip()
        example['id'] = id + '_' + str(index)
        index += 1
        examples.append(example)

        for post in soup.find_all('post'):
            cls = post.find_all('class')[0].string.strip()
            example = {}
            example['class_1'] = bhatiaToOurTag[cls]
            example['sentence'] = ''.join(['+' for s in range(index)]) + post.rcontent.string.strip()
            example['id'] = id + '_' + str(index)
            index += 1
            examples.append(example)

    dataCleaning.preetify(examples)
    preprocessing.addTokens(examples)
    preprocessing.addLemmas(examples)

    for example in examples:
        example['pos'] = [p for w,p in nltk.pos_tag(example['tokens'])]

    return examples