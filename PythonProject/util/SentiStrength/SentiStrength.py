import subprocess
import shlex
from constants import SENTISTRENGTH_LOCATION
import re


# The method returns a triple, where:
#   - first value: value of positive sentiment (1 - weak, 5 - strong)
#   - second value: value of negative sentiment (1 - weak, 5 - strong)
#   - third value: binary value (1- positive, -1 - negative)
def rateSentiStrengthSentiment(sentiString):
    #open a subprocess using shlex to get the command line string into the correct args list format

    p = subprocess.Popen(shlex.split("java -jar {}/SentiStrengthCom.jar stdin sentidata {}/Data2015English/ binary".format(SENTISTRENGTH_LOCATION, SENTISTRENGTH_LOCATION)),
                         stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
    #communicate via stdin the string to be rated. Note that all spaces are replaced with +
    stdout_text, stderr_text = p.communicate(bytes(sentiString.replace(" ","+"), encoding="utf-8"))
    #remove the tab spacing between the positive and negative ratings. e.g. 1    -5 -> 1-5
    stdout_text = stdout_text.rstrip()

    if len(stderr_text) > 0:
        raise Exception("Error while accessing SentiStrength: {}".format(stderr_text))

    output = re.split(r'\t+', stdout_text.decode("utf-8"))
    return (int(output[0]), int(output[1]), int(output[2]))