import csv
from preprocessing import preprocessing, dataCleaning
from constants import CORPUS_LOCATION, CORPUS_EWDC_COMPLAIN, CORPUS_EWDC_ENGAGE
import string
import nltk

# reply_to_sid is the id of the last sentence in the post
from util.bhatiaImport import importBhatiaDataset
from util.swdaImport import importSWDADataset


def getParentPostText(reply_to_sid, examples):
    text = ""
    postId = examples[reply_to_sid]['id']

    while examples[reply_to_sid]['id'] == postId and reply_to_sid > 0:
        text = examples[reply_to_sid]['sentence'] + " " + text
        reply_to_sid -= 1

    return text

# example can be an arbitrary sentence from a post
def getPostText(example, examples):
    postId = example['id']

    # go to first sentence
    while example['id'] == postId:
        if (example['sid'] == 0):
            break
        example = examples[example['sid'] - 1]

    # get text to the end of the post
    text = ""
    while len(examples) > example['sid'] + 1 and examples[example['sid'] + 1]['id'] == postId:
        example = examples[example['sid'] + 1]
        text += " " + example['sentence']

    return text

# This returns only the conversation path text, not the whole thread text, which is a tree.
def getConversationText(example, examples):
    # current sentence
    text = example['sentence']

    # all sentences and text before
    curExample = example
    while curExample['sid'] - 1 >= 0:
        prevExample = examples[curExample['sid'] - 1]
        if (prevExample['id'] == curExample['id']):
            curExample = prevExample
            text = curExample['sentence'] + " " + text
        elif (prevExample['sid'] == curExample['reply_to_sid']):
            curExample = prevExample
            text = curExample['sentence'] + " " + text
        else:
            break

    # all sentences and text after
    curExample = example
    while curExample['sid'] + 1 < len(examples):
        nextExample = examples[curExample['sid'] + 1]
        if (nextExample['id'] == curExample['id']):
            curExample = nextExample
            text = text + " " + curExample['sentence']
        elif (curExample['sid'] == nextExample['reply_to_sid']):
            curExample = nextExample
            text = text + " " + curExample['sentence']
        else:
            break

    return text

def getRootPostText(example, examples):

    # get the sid of the last sentence in the root post
    lastRootExample = example

    while (lastRootExample['reply_to_sid'] != lastRootExample['sid']):
        lastRootExample = examples[lastRootExample['reply_to_sid']]


    # go to the last sid in the post (if the parameter is a sentence from a root post)
    postId = lastRootExample['id']
    while (lastRootExample['sid'] + 1 < len(examples) and examples[lastRootExample['sid'] + 1]['id'] == postId):
        lastRootExample = examples[lastRootExample['sid'] + 1]

    return getParentPostText(lastRootExample['sid'], examples)


# This function transforms lists of strings (tokens, lemmas) from examples into encoded texts by integers
# and returns a dictionary to map from word to index. Indexes lists are added to examples to lists named indexed_list_name.
#
# num_pad_max - number of most appearing words to truncate
# words_max - number of maximum words to have in dictionary
#
# Words indexes start with 1, from the highest appearing word to the lowest.
#
def texts_to_indexes(examples, list_name, indexed_list_name = "indexed", num_pad_max = 0, words_max = None):
    #get counts
    token_count = {}
    for example in examples:
        for token in example[list_name]:
            token_count[token] = token_count.get(token, 0) + 1

    # order, pad and take max
    word_counts = sorted(list(token_count.items()), key = lambda x: x[1], reverse= True)[num_pad_max:]
    if words_max is not None:
        word_counts = word_counts[:words_max]

    #create word_index
    word_index = dict( [(w,i) for i,w in enumerate([w for w, _ in word_counts], start = 1)] )

    #encode texts
    encoded_texts = []
    for example in examples:
        example[indexed_list_name] = [word_index[token] for token in example[list_name] if token in word_index]

    return word_index

# Imported dataset has attributes:
#   - sentence, id, classes
# Pretify function
#   - introduces sid, reply_to_sid, reply_to_id
# Lastly
#   - newly added are tokens, lemmas, pos
def importDataStandard(defaultCorpus = CORPUS_LOCATION, addEWDCComplain = False, addEWDCEngage = False, addBhatiaClasses = None, addSWDAClassses = None):
    examples = []

    if (addEWDCComplain):
        with open(CORPUS_EWDC_COMPLAIN, 'r') as f:
            reader = csv.DictReader(f, delimiter=',')
            for example in reader:
                examples.append(example)

    if (addEWDCEngage):
        with open(CORPUS_EWDC_ENGAGE, 'r') as f:
            reader = csv.DictReader(f, delimiter=',')
            for example in reader:
                examples.append(example)

    with open(defaultCorpus, 'r') as f:
        reader = csv.DictReader(f, delimiter=',')
        for example in reader:
            examples.append(example)

    dataCleaning.preetify(examples)
    preprocessing.addTokens(examples)
    preprocessing.addLemmas(examples)

    for example in examples:
        example['pos'] = [p for w,p in nltk.pos_tag(example['tokens'])]

    if addBhatiaClasses:
        bhatiaExamples = importBhatiaDataset(addBhatiaClasses)
        examples += bhatiaExamples

    if addSWDAClassses:
        swdaExamples = importSWDADataset(addSWDAClassses)
        examples += swdaExamples

    return examples

def removePunctuation(tokens):
    return [x for x in tokens if x not in string.punctuation]


def test_split(examples, features_list_name, label_list_name, test_split):
    X = [example[features_list_name] for example in examples]
    labels = [example[label_list_name] for example in examples]

    X_train = X[:int(len(X) * (1 - test_split))]
    y_train = labels[:int(len(X) * (1 - test_split))]

    X_test = X[int(len(X) * (1 - test_split)):]
    y_test = labels[int(len(X) * (1 - test_split)):]

    return (X_train, y_train), (X_test, y_test)



# Leaf posts are the ones that have no answers - t.i. their id never appears in 'reply_to_id'
def getLeafPostIds(examples):
    nonLeafIds = set([example['reply_to_id'] for example in examples if example['reply_to_id'] != example['id']])
    return set([example['id'] for example in examples if example['id'] not in nonLeafIds])

def getRootPostIds(examples):
    return set([path[0] for path in getConversationPaths(examples)])


def getConversation(examples, leaf_postId):
    # find a postId sentence and retrieve reply_to_id
    postIdExample = [example for example in examples if example['id'] == leaf_postId][0]

    # if it is not a root, find further, else return
    if postIdExample['reply_to_id'] == leaf_postId:  # is root
        return [leaf_postId]
    else:
        return getConversation(examples, postIdExample['reply_to_id']) + [leaf_postId]

# conversation is a list of consequent posts. Root post is indexed with 0
def getConversationPaths(examples):
    conversations = []

    for postId in getLeafPostIds(examples):
        conversations.append(getConversation(examples, postId))

    return conversations

def getPostExamples(postId, examples):
    postExamples = []

    for example in examples:
        if (example['id'] == postId):
            postExamples.append(example)

    return postExamples

def getAllPostIds(examples):
    return list(set([example['id'] for example in examples]))