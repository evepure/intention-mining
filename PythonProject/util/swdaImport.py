from constants import SWDA_LOCATION
import glob
import csv
import re
import nltk

# See importantDatasetStandard in util.datasetUtil for more info
from preprocessing import preprocessing, dataCleaning


def mapTags(act_tag):
    """
    Seeks to duplicate the tag simplification described at the
    Coders' Manual: http://www.stanford.edu/~jurafsky/ws97/manual.august1.html
    """
    d_tags = []
    tags = re.split(r"\s*[,;]\s*", act_tag)
    for tag in tags:
        if tag in ('qy^d', 'qw^d', 'b^m'):
            pass
        elif tag == 'nn^e':
            tag = 'ng'
        elif tag == 'ny^e':
            tag = 'na'
        else:
            tag = re.sub(r'(.)\^.*', r'\1', tag)
            tag = re.sub(r'[\(\)@*]', '', tag)
            if tag in ('qr', 'qy'):
                tag = 'qy'
            elif tag in ('fe', 'ba'):
                tag = 'ba'
            elif tag in ('oo', 'co', 'cc'):
                tag = 'oo_co_cc'
            elif tag in ('fx', 'sv'):
                tag = 'sv'
            elif tag in ('aap', 'am'):
                tag = 'aap_am'
            elif tag in ('arp', 'nd'):
                tag = 'arp_nd'
            elif tag in ('fo', 'o', 'fw', '"', 'by', 'bc'):
                tag = 'fo_o_fw_by_bc'
        d_tags.append(tag)
    # Dan J says (p.c.) that it makes sense to take the first;
    # there are only a handful of examples with 2 tags here.
    return d_tags[0]

swdaToOurTag = {"sd":"ASSERT", "ny":"AGREE", "na":"AGREE", "nn":"AGREE", "ng":"AGREE", "h":"GUESS", "aap_am":"GUESS", "no":"GUESS",
                "qy":"DIRECT", "qw":"DIRECT", "qy^d":"DIRECT", "bh":"DIRECT", "^q":"DIRECT", "qw^d":"DIRECT", "qo":"DIRECT", "ad":"DIRECT", "oo_co_cc":"ENGAGE", "fa":"APOLOGIZE", "ft":"THANK",
                "b":"TO_OMIT", "ba":"TO_OMIT", "sv":"TO_OMIT", "aa":"TO_OMIT", "%":"TO_OMIT", "x":"TO_OMIT", "fc":"TO_OMIT", "nn":"TO_OMIT", "bk":"TO_OMIT", "fo_o_fw_by_bc":"TO_OMIT",
                "bf":"TO_OMIT", "^2":"TO_OMIT", "b^m":"TO_OMIT", "qh":"TO_OMIT", "^h":"TO_OMIT", "ar":"TO_OMIT", "br":"TO_OMIT", "fp":"TO_OMIT", "qrr":"TO_OMIT",
                "arp_nd":"TO_OMIT", "t3":"TO_OMIT", "t1":"TO_OMIT", "bd":"TO_OMIT", "^g":"TO_OMIT"}


def  importSWDADataset():
    examples = []

    for filename in glob.glob(SWDA_LOCATION):
        #print("Importing {}".format(filename))
        with open(filename, 'r') as file:
            reader = csv.DictReader(file, delimiter=',')
            previousTag = ""
            previousCaller = "A"
            sentence_prefix = ""

            for reader_example in reader:
                #if not "3796" in reader_example['conversation_no']:
                #    continue
                sentence = re.sub(' +', ' ', re.sub(r"([+/\}\[\]]|\{\w)", "", reader_example['text']))

                if sentence == "":
                    continue

                example = {}

                if reader_example['act_tag'].startswith('+'):
                    example['class_1'] = mapTags(previousTag)
                else:
                    example['class_1'] = mapTags(reader_example['act_tag'])
                    previousTag = reader_example['act_tag']

                if not reader_example['caller'] == previousCaller:
                    sentence_prefix += "+"
                    previousCaller = reader_example['caller']
                example['sentence'] = sentence_prefix + sentence
                example['id'] = reader_example['conversation_no'] + "_" + reader_example['utterance_index']
                if (example['sentence'].strip() != ''):
                    examples.append(example)
                    #print(example["sentence"])
                    #print(example)





    print("preetify")
    dataCleaning.preetify(examples)
    print("adding tokens")
    preprocessing.addTokens(examples)
    print("adding lemmas")
    preprocessing.addLemmas(examples)

    print(len(examples))

    for example in examples:
        example['pos'] = [p for w,p in nltk.pos_tag(example['tokens'])]

    examples = list(filter(lambda example: len(example['pos']) > 0 and len(example['tokens']) > 0, examples))
    print(len(examples))

    for example in examples:
        example['class_1'] = swdaToOurTag[example['class_1']]

    return examples
