BHATIA_LOCATION = '../../Data/Bhatia/Ubuntu/*.xml'
CORPUS_LOCATION = '../../Data/Reddit/corpus.csv'
CORPUS_VAL_LOCATION = '../../Data/Reddit/corpus_rcis2018.csv'
CORPUS_ADDITIONAL_LOCATION = '../../Data/WikiDiscussion/EWDC_intentions.csv'
CORPUS_ADDITIONAL_LOCATION_ENGAGE_ONLY = '../../Data/WikiDiscussion/EWDC_intentions_engageOnly.csv'
CORPUS_EWDC_COMPLAIN = '../../Data/WikiDiscussion/EWDC_intentions_complain.csv'
CORPUS_EWDC_ENGAGE = '../../Data/WikiDiscussion/EWDC_intentions_engage.csv'
SWDA_LOCATION = '../../Data/Switchboard/swda/*/*.csv'

# Set absolute path on yout system (inside this project)
SENTISTRENGTH_LOCATION = '/Users/slavkoz/Projects/ResearchProjects/intention-mining/PythonProject/util/SentiStrength/'
# (OPTIONAL) Download Glove from: https://nlp.stanford.edu/projects/glove/
GLOVE_DIR = '/Users/slavkoz/Downloads/glove.6B/'
# (OPTIONAL) Download and install CRF implementation from http://www.chokkan.org/software/crfsuite/
CRF_MODEL = 'models/model.crf.tagger'