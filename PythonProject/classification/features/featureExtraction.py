from classification.features.features import *
from classification.features.bhatiaFeatures import containsOneOf_5w1h, textLength, textStemmedLength, \
    textStemmedUniqueLength, textUniqueLength, sentencePos, postPos, hasQuestionmark, hasExclamationMark, hasThank, \
    hasPositiveFeedback, sentimentValue, sentenceNormPos, hasDuplicateWords, isQuote, conversationSim, initSim
from classification.features.ngramFeatures import NGram
from classification.features.precalculatedNgramFeatures import PNGram
from util.datasetUtil import getParentPostText

NGRAM_1 = None
NGRAM_2 = None
NGRAM_3 = None
PUNCTUATION_KEYS = None
VERBTENSE_KEYS = None
PRONOMINAL_KEYS = None
STAT_LIU = None
STAT_WILSON = None
POSTAG_PAIRS = None

def featureExtraction(examples, verbose = False, featureGroups = [1,2,3], usePreloadedFeatureKeys = False, classesToCreateFeatures = None):
    if not usePreloadedFeatureKeys:
        global NGRAM_1
        global NGRAM_2
        global NGRAM_3
        global PUNCTUATION_KEYS
        global VERBTENSE_KEYS
        global PRONOMINAL_KEYS
        global STAT_LIU
        global STAT_WILSON
        global POSTAG_PAIRS

    if (1 in featureGroups):
        ngramPos = PNGram(examplesValue="pos")
    if (2 in featureGroups):
        if not usePreloadedFeatureKeys:
            NGRAM_1 = NGram(1, examples, minOccurence=3)
            NGRAM_2 = NGram(2, examples, minOccurence=3)
            NGRAM_3 = NGram(3, examples, minOccurence=3)
        ngramTokens = PNGram()

    # prepare features
    features = []
    featureNames = []
    classes = []

    if not usePreloadedFeatureKeys:
        PUNCTUATION_KEYS = list(hasPunctuations(examples[0]).keys())
        VERBTENSE_KEYS = list(verbTense(examples[0]).keys())
        PRONOMINAL_KEYS = list(pronominalCues(examples[0]).keys())
        STAT_LIU = list(statisticsOpinionWordsLiu(examples[0]).keys())
        STAT_WILSON = list(statisticsOpinionWordsWilson(examples[0], weakIncluded=False).keys())
        POSTAG_PAIRS = posTagFeaturePairs(examples)
    for i in range(0, len(examples)):
        #if i < 80500: # TODO: enable assert below
        #    continue
        #print(str(examples[i]["id"]) + " " + str(examples[i]["sid"]))

        example = examples[i]
        if classesToCreateFeatures and not example["class_1"] in classesToCreateFeatures:
            continue

        item = []

        # Discourse Features meta-information extracted from the content
        if (1 in featureGroups):
            item.append(frequency5w1h(example))  # ELENA TWITTER
            item.append(numberOfNegations(example))
            item.append(verbPos(example))
            item.append(hasFutureVerb(example))
            item.append(hasNounBeforeVerb(example))
            item += [verbTense(example)[key] for key in VERBTENSE_KEYS]
            item += [statisticsOpinionWordsLiu(example)[key] for key in STAT_LIU]  # ELENA TWITTER
            item += [statisticsOpinionWordsWilson(example, weakIncluded=False)[key] for key in STAT_WILSON]  # ELENA TWITTER
            for f in modalsFunctionGenerator():  # ELENA TWITTER
                item.append(f(example))  # --- /// ---
            item.append(hasExclamationMark(example))
            item.append(hasQuestionmark(example))
            item += [hasPunctuations(example)[key] for key in PUNCTUATION_KEYS]  # ELENA TWITTER
            item += [pronominalCues(example)[key] for key in PRONOMINAL_KEYS]  # ELENA TWITTER
            item.append(hasLink(example))  # ELENA TWITTER
            item.append(numberOfEmoticons(example))  # ELENA TWITTER
            item.append(numberOfUnicodeEmoticons(example))  # ELENA TWITTER
            item.append(hasTitleWords(example))
            item.append(containtsCapsWord(example))
            item.append(hasDuplicateWords(example))
            #item.append(hasThank(example))
            item.append(textLength(example['tokens']))
            item.append(textUniqueLength(example['tokens']))
            item.append(textStemmedLength(example['tokens']))
            item.append(textStemmedUniqueLength(example['tokens']))
            item.append(numberOfInterjections(example))  # ELENA TWITTER
            item += ngramPos.getFeatureVectorValues(example)
            for posTagPair in POSTAG_PAIRS:
                item.append("/".join(example["pos"]).count(posTagPair))
            # item.append(containsOneOf_5w1h(example))
            # item.append(hasPositiveFeedback(example))
            # item.append(sentimentValue(example))

        # Content shape features
        if (2 in featureGroups):
            item += NGRAM_1.getFeatureVectorValues(example)
            item += NGRAM_2.getFeatureVectorValues(example)
            item += NGRAM_3.getFeatureVectorValues(example)
            item += ngramTokens.getFeatureVectorValues(example)

        # Conversation Features features exploiting the conversation's structure and its content
        if (3 in featureGroups):
            #print("1")
            item.append(postPos(example, examples))
            #print("2")
            item.append(sentencePos(example, examples))
            #print("3")
            item.append(sentenceNormPos(example, examples))
            #print("4")
            item.append(isQuote(example, examples))
            #print("5")
            item.append(cosineSim(example['sentence'], getParentPostText(example['reply_to_sid'], examples)))
            #print("6")
            item.append(conversationSim(example, examples))
            #print("7")
            item.append(initSim(example, examples))

        features.append(item)
        classes.append(example['class'])

        if verbose or i % 100 == 0:
            print(("Done {} examples. Created {} feature examples.".format(i, len(features))))

    # Add feature names
    if (True):
        if (1 in featureGroups):
            featureNames.append("frequency5w1h")
            featureNames.append("numberOfNegations")
            featureNames.append("verbPos")
            featureNames.append("hasFutureVerb")
            featureNames.append("hasNounBeforeVerb")
            featureNames += ["HAS_" + name for name in VERBTENSE_KEYS]
            featureNames += ["COUNT_" + name for name in STAT_LIU]
            featureNames += ["COUNT_" + name for name in STAT_WILSON]
            featureNames += ["COUNT_" + name for name in
                             ["can", "cannot", "can't", "cant", "could", "couldn't", "couldnt", "must", "mustn't",
                              "mustnt", "might", "mightn't", "mightnt", "should",
                              "shouldn't", "shouldnt", "may", "mayn't", "maynt"]]
            featureNames.append("hasExclamationMark")
            featureNames.append("hasQuestionmark")
            featureNames += ["HAS_" + name for name in PUNCTUATION_KEYS]
            featureNames += ["COUNT_" + name for name in PRONOMINAL_KEYS]
            featureNames.append("hasLink")
            featureNames.append("numberOfEmoticons")
            featureNames.append("numberOfUnicodeEmoticons")
            featureNames.append("hasTitleWords")
            featureNames.append("containtsCapsWord")
            featureNames.append("hasDuplicateWords")
            #featureNames.append("hasThank")
            featureNames.append("textLength")
            featureNames.append("textUniqueLength")
            featureNames.append("textStemmedLength")
            featureNames.append("textStemmedUniqueLength")
            featureNames.append("numberOfInterjections")
            featureNames += ["NGRAM_P_" + name for name in ngramPos.getFeatureVectorNames()]
            for posTagPair in POSTAG_PAIRS:
                featureNames.append("POSPAIR_"+posTagPair)
            #featureNames.append("containsOneOf_5w1h")
            #featureNames.append("hasPositiveFeedback")
            #featureNames.append("sentimentValue")



        if (2 in featureGroups):
            featureNames += ["NGRAM_1_" + name for name in NGRAM_1.getFeatureVectorNames()]
            featureNames += ["NGRAM_2_" + name for name in NGRAM_2.getFeatureVectorNames()]
            featureNames += ["NGRAM_3_" + name for name in NGRAM_3.getFeatureVectorNames()]
            featureNames += ["NGRAM_T_" + name for name in ngramTokens.getFeatureVectorNames()]

        if (3 in featureGroups):
            featureNames.append("postPos")
            featureNames.append("sentencePos")
            featureNames.append("sentenceNormPos")
            featureNames.append("isQuote")
            featureNames.append("cosineSim")
            featureNames.append("conversationSim")
            featureNames.append("initSim")

        # Check if length match
        assert len(featureNames) == len(features[0])

    return (features, featureNames, classes)