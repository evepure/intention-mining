import re
import nltk
import string
from sklearn.feature_extraction.text import TfidfVectorizer
from nltk.tokenize import word_tokenize
from nltk.stem.wordnet import WordNetLemmatizer
from nltk.corpus import words
import enchant


stemmer = nltk.stem.porter.PorterStemmer()

# Return 1 if the input example contains at least one link
# Otherwise 0
def hasLink(example):
    result = re.search(r'https?://[^\s<>"]+|www\.[^\s<>"]+', example["sentence"])
    if result is None:
        return 0
    return 1

# Checks whether the sentence may refer to imperative
def hasNounBeforeVerb(example):
    pos = example['pos']
    nounLocs= []
    if 'NN' in pos:
        nounLocs.append(pos.index('NN'))
    if 'NNS' in pos:
        nounLocs.append(pos.index('NNS'))
    if 'NNP' in pos:
        nounLocs.append(pos.index('NNP'))
    if 'NNPS' in pos:
        nounLocs.append(pos.index('NNPS'))
    if len(nounLocs) == 0:
        return 2
    nounLoc = min(nounLocs)

    if 'VB' in pos:
        if pos.index('VB') < nounLoc:
            return 0
    return 1


# Return a dictionary with the keys ["?", "!", ":", ".."]
# The values are either 0 or 1 for presence or absence of the symbol
def hasPunctuations(example):
    # the result dictionary
    result = {"?": 0, "!": 0, ":": 0, "..": 0}
    sentence = re.sub(r'https?://[^\s<>"]+|www\.[^\s<>"]+', '', example["sentence"])
    if "?" in sentence:
        result["?"] = 1
    if "!" in sentence:
        result["!"] = 1
    if ": " in sentence:
        result[":"] = 1
    if ".." in sentence:
        result[".."] = 1
    return result


# Return the absolute frequency of 5w1h in the sentence
# ["what", "where", "when", "why", "who", "how"]
def frequency5w1h(example):
    sentence = example["sentence"].lower()
    pattern = re.compile("(?:^|\s)(what|whats|what's|when|where|why|who|how)(?:\s|$)")
    result = pattern.findall(sentence)
    return len(result)


## DATASETS IMPORT
EMOTICONS = []
with open('../Data/emoticons.txt', 'r') as f:
    for line in f:
        EMOTICONS.append(line.strip())
INTERJECTIONS = []
with open('../Data/interjections.txt', 'r') as f:
    for line in f:
        INTERJECTIONS.append(line.strip())

POSITIVE_TERMS_LIU = []
with open('../Data/positive-words.txt', 'r') as f:
    POSITIVE_TERMS_LIU = set([w.strip() for w in f if (not w.startswith(';')) and w.strip() is not ''])
NEGATIVE_TERMS_LIU = []
with open('../Data/negative-words.txt', 'r', encoding="mac_roman") as f:
    NEGATIVE_TERMS_LIU = set([w.strip() for w in f if (not w.startswith(';')) and w.strip() is not ''])

# Dictionaries containing the opinion words as keys,
# and as values 0 if the subjectivity is weak and 1 if subjectivity is strong
# Ignore all the entries with neutral polarity
POSITIVE_TERMS_WILSON = {}
NEGATIVE_TERMS_WILSON = {}
with open('../Data/subjclueslen1-HLTEMNLP05.tff', 'r') as f:
    for line in f:
        #line pre-processing
        line = re.sub('type=|len=|word1=|pos1=|stemmed1=|priorpolarity=', '', line)
        parts = line.split(' ')

        #extract the useful info
        subj = 0 if parts[0].strip() == "weaksubj" else 1
        word = parts[2].strip()
        polarity = parts[5].strip()

        #build the dictionaries
        if polarity == "negative":
            NEGATIVE_TERMS_WILSON[word] = subj
        elif polarity == "positive":
            POSITIVE_TERMS_WILSON[word] = subj
        else:
            continue




# Returns the number of emoticons in a sentence
def numberOfEmoticons(example):
    emoticons = 0
    sentence = re.sub(r'https?://[^\s<>"]+|www\.[^\s<>"]+', '', example["sentence"])

    for emoticon in EMOTICONS:
        for m in re.finditer(re.escape(emoticon), sentence):
            # Check for false positives as 'xp'
            # These emoticons could appear as part of other strings
            # For this reason, check for a space before and after
            start = m.start()
            stop = start + len(emoticon)
            if emoticon.isalpha() and (sentence[start-1] is not ' ' and sentence[stop] is not ' '):
                continue
            emoticons += 1

    return emoticons


# Returns the number of unicode emoticons
def numberOfUnicodeEmoticons(example):
    noEmoticons = 0
    text = example["sentence"]

    for c in text:
        # The unicode emoticons are recognized through their codes
        hexc = str(hex(ord(c))).lower()
        if hexc.startswith('0xf0') or hexc.startswith('0xe2'):
            noEmoticons += 1

    # Transform it back to utf-8
    return noEmoticons


# Returns the number of emoticons in a sentence
def numberOfInterjections(example):
    interjections = 0
    sentence = example['sentence']
    tokens = example['tokens']

    for interjection in INTERJECTIONS:
        if (len(interjection.split(" ")) > 1):
            interjections += len([m.start() for m in re.finditer(re.escape(interjection), sentence)])
        else:
            try:
                interjections += len([x for x in tokens if x.decode('utf-8') == interjection.decode('utf-8')])
            except:
                pass

    return interjections

### Cosine similarity between strings ###
def stem_tokens(tokens):
    return [stemmer.stem(item) for item in tokens]

# Remove punctuation, lowercase, stem
def normalize(text):
    remove_punctuation_map = dict((ord(char), None) for char in string.punctuation)
    return stem_tokens(nltk.word_tokenize(text.lower().translate(remove_punctuation_map)))

def cosineSim(text1, text2):
    vectorizer = TfidfVectorizer(tokenizer=normalize, stop_words='english')
    retVal = 0
    #need to do this because exception is raised for some inputs
    try:
        tfidf = vectorizer.fit_transform([text1, text2])
        retVal = ((tfidf * tfidf.T).A)[0,1]
    except:
        pass
    return retVal


# Returns a boolean regarding whether a sentence contains a word with capitals only
englishWords = set(words.words())
def containtsCapsWord(example):
    tokens = word_tokenize(example['sentence'])

    def check(word):
        # The length has to be bigger than 3, the word should be characters and an english word
        # to avoid abbrevations
        if (len(word) > 3 and word == word.upper() and word.lower() in englishWords):
            return True
        else:
            return False
    return len([x for x in tokens if check(x)]) > 0

# Returns the index of word representing a first verb in a sentence
# If a verb is not found, -1 is returned
def verbPos(example):
    posTags = example['pos']
    if posTags.count('VB') > 0:
        return posTags.index('VB')
    else:
        return -1

# Returns a list of functions that check how many times a modal appear in a sentence
modals = ["can", "cannot", "can't", "cant", "could", "couldn't", "couldnt", "must", "mustn't", "mustnt", "might", "mightn't", "mightnt", "should",
              "shouldn't", "shouldnt", "may", "mayn't", "maynt"]
def modalsFunctionGenerator():

    functions = []

    for modal in modals:
        def f1(modal):
            def f2(example):
                return example['tokens'].count(modal)
            return f2
        functions.append(f1(modal))

    return functions

# Returns the number of negating structures found
def numberOfNegations(example):
    pattern = re.compile("(?:^|\s)(aren't|arent|can't|cannot|cant|couldn't|couldnt|don't|dont|isn't|isnt|never|not|no|won't|wont|wouldn't|wouldnt)(?:\s|$)")
    sentence = example["sentence"].lower()
    result = pattern.findall(sentence)
    return 0 if result is None else len(result)

# Returns 1 if the sentence contains future verbal cues and 0 if not
def hasFutureVerb(example):
    pattern = re.compile("(?:^|\s)(will|won't|wont|'ll|going to|gonna)(?:\s|$)")
    sentence = example["sentence"].lower()
    result = pattern.findall(sentence)
    return 1 if result is not None and len(result) > 0 else 0

# Features re the form of the verb: past or present or ing
def verbTense(example):
    result = {"hasPastV": 0, "hasIngV": 0, "hasImperativeV":0}

    posTags = list(zip(example["tokens"], example["pos"]))
    uniquePostTags = set([x[1] for x in posTags])

    if "VBD" in uniquePostTags:
        result["hasPastV"] = 1

    # Here care should be given to the situations when past participle
    # is used for past forms of infinitif or as adjectives
    # The only correct situations are those when used in present perfect tense (have gone),
    # past perfect tense (had gone) or past forms of modal (could have taken)
    auxs = set(["have", "had", "haven't", "havent", "hadn't", "hadnt"])
    if "VBN" in uniquePostTags:

        for i in range(1, len(posTags)):
            if posTags[i][1] == "VBN":
                if posTags[i-1][0] in auxs or (i > 1 and posTags[i-2][0] in auxs):
                    result["hasPastV"] = 1

    if "VBG" in uniquePostTags:
        result["hasIngV"] = 1

    # Imperative
    # !!!! Take a second look to improve the code

    # For now check if the first word is a verb
    try:
        if posTags[0][1] == "VB":
            lemmatizer = WordNetLemmatizer()

            # make sure there is not a false positive because of a question
            if '?' not in example["sentence"]:
                # exclude those which are not infinitif verbs
                # example "Told my boss that"
                # Lemmatize a second time by also telling this time the particle is a verb
                if posTags[0][0] ==  lemmatizer.lemmatize(example["lemmas"][0], 'v'):
                    #also check if the first verb is not a particle for another tense like have or do
                    if posTags[1][1] != "VBN" and posTags[2][1] != "VBN":
                        result["hasImperativeV"] = 1
    except:
        pass

    return result

# Returns the number of specific pronominal cues existing in the sentence
def pronominalCues(example):
    result = {"has_1stperson_sg": 0, "has_1stperson_pl": 0, "has_2ndperson": 0, "has_3rdperson": 0}
    for t in example["tokens"]:
        if t in ['i', 'me', 'my', 'mine', 'myself']:
            result["has_1stperson_sg"] += 1

        if t in ['we', 'us', 'our', 'ours', 'ourselves']:
            result["has_1stperson_pl"] += 1

        if t in ['you', 'u', 'your', 'yours', 'yourself', 'yourselves', 'ur']:
            result["has_2ndperson"] += 1

        if t in ["he", "she", "they", "it", "her", "him", "his", "hers", "theirs", "their", "its", "them"]:
            result["has_3rdperson"] += 1
    return  result

# function based on Liu et al. lexicon
def statisticsOpinionWordsLiu(example):
    result = {"numberPosL":.0, "numberNegL":.0, "ratioPosL":.0, "ratioNegL":.0}

    stemWords = set([stemmer.stem(item) for item in example["tokens"]])
    for t in stemWords:
        if t in POSITIVE_TERMS_LIU:
            result["numberPosL"] += 1
            result["ratioPosL"] += 1
        if t in NEGATIVE_TERMS_LIU:
            result["numberNegL"] += 1
            result["ratioNegL"] += 1

    totalWords = len(stemWords)
    result["ratioPosL"] /= totalWords
    result["ratioNegL"] /= totalWords

    return  result

# function based on Wilson lexicon
def statisticsOpinionWordsWilson(example, weakIncluded=False):
    result = {"numberPosW":.0, "numberNegW":.0, "ratioPosW":.0, "ratioNegW":.0}

    stemWords = set([stemmer.stem(item) for item in example["tokens"]])
    for t in stemWords:
        if t in POSITIVE_TERMS_WILSON:
            #If only strong opinion words are counted then ignore if marked as weak subj.
            if not weakIncluded and POSITIVE_TERMS_WILSON[t] == 0:
                continue
            result["numberPosW"] += 1
            result["ratioPosW"] += 1
        if t in NEGATIVE_TERMS_WILSON:
            # If only strong opinion words are counted then ignore if marked as weak subj.
            if not weakIncluded and NEGATIVE_TERMS_WILSON[t] == 0:
                continue
            result["numberNegW"] += 1
            result["ratioNegW"] += 1

    totalWords = len(stemWords)
    result["ratioPosW"] /= totalWords
    result["ratioNegW"] /= totalWords

    return  result

# Checks if it has titled words; return 0 or 1
def hasTitleWords(example):
    n = 0
    for t in example['tokens']:
        if t[0].isupper():
            n += 1
    return n / len(example['tokens'])

def has_imperative(example):
    endict = enchant.Dict('en_US')
    auxs = set(["have", "had", "haven't", "havent", "hadn't", "hadnt", "do", "don't", "does", "doesn't", "dont", "doesnt", "did", "didnt", "will", "won't","wont", "would","wouldn't","wouldnt", "is", "are", "isn't", "aren't", "isnt", "arent"])
    modals = set(["can", "cannot", "can't", "cant", "could", "couldn't", "couldnt", "must", "mustn't", "mustnt", "might", "mightn't", "mightnt", "should",
              "shouldn't", "shouldnt", "may", "mayn't", "maynt"])

    for i in range(len(example["pos"])):
        p = example["pos"][i]
        t = example["tokens"][i].lower()
        l = example["lemmas"][i]
        #i == 0 or
        if p == 'V' and 'ing' not in t and (i== 0 or example["pos"][i-1] in [',', 'U', 'E','$']):
            if (l in auxs or l in modals) and '?' in example['sentence']:
                return 0
            if endict.check(t):
                if t == l:
                    return 1
            #else:
            #    return 1
    return 0

# POS tag pairs in a dataset
def posTagFeaturePairs(examples):
    posPairs = set()
    for example in examples:
        prevPos = example["pos"][0]
        for curPos in example["pos"][1:]:
            posPairs.add(prevPos+"/"+curPos)
    return list(posPairs)
