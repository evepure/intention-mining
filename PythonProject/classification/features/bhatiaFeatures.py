import nltk
import re
from util.SentiStrength.SentiStrength import rateSentiStrengthSentiment
from util.datasetUtil import getParentPostText, getPostText, getConversationText, getRootPostText
from util.cosineSimilarity import cosineSimilarity

"""
This file recreates features according to the paper "Classifying User Messages For Managing Web Forum Data" by
Sumit Bhatia, et. al., which was published at WebDB 2012.

The full list of features are as follows:
    Content-based features:
        - IsQuote                   - DONE (isQuote)
        - TitleSim                  - Not Applicable
        - InitSim                   - DONE (initSim)
        - ThreadSim                 - DONE (conversationSim)
        - QuestionMark              - DONE (hasQuestionmark)
        - Duplicate                 - DONE (hasDuplicateWords)
        - 5W1H                      - DONE (containsOneOf_5w1h)

    Structural features:
        - AbsPos                    - DONE (sentencePos, postPos)
        - NormPos                   - DONE (sentenceNormPos)
        - PostLength                - DONE (textLength)
        - PostLengthUnique          - DONE (textUniqueLength)
        - PostLengthStemmed         - DONE (textStemmedLength)
        - PostLengthUniqueStemmed   - DONE (textStemmedUniqueLength)

    User features:
        - UserPostCount             - Not Applicable
        - IsStarter                 - Not Applicable
        - UserAuth                  - Not Applicable

    Sentiment-based features:
        - Thank                     - DONE (hasThank)
        - ExclamationMark           - DONE (hasExclamationMark)
        - Positivve Feedback        - DONE (hasPositiveFeedback)
        - SentimentScore            - DONE (sentimentValue)
"""


### Features regarding the length of a text ###
stemmer = nltk.stem.porter.PorterStemmer()

# version 1 original #words without any preprocessing
def textLength(listWords):
    if listWords is None or len(listWords) == 0:
        return 0
    return len(listWords)

# version 2 #words considering only unique terms
def textUniqueLength(listWords):
    return textLength(set(listWords))

# version 3 #words considering the stemmed words
def textStemmedLength(listWords):
    return textLength([stemmer.stem(item) for item in listWords])

# version 4 #words considering the stemmed and unique words
def textStemmedUniqueLength(listWords):
    return textLength(set([stemmer.stem(item) for item in listWords]))

# Returns the depth of the post in the thread.
def postPos(example, examples):
    postIds = [example['id']]

    while example['sid'] != example['reply_to_sid']:
        example = examples[example['reply_to_sid']]
        postIds.append(example['id'])

    return len(set(postIds)) - 1


# Returns the position of a sentence within a post. First sentence in a post has position of 1.
def sentencePos(example, examples):
    postId = example['id']
    sentencePos = 0

    while example['id'] == postId and example['sid'] > 0:
        example = examples[example['sid'] - 1]
        sentencePos += 1

    return sentencePos

# Returns the normalized position of a sentence within a post.
def sentenceNormPos(example, examples):
    postId = example['id']
    sentencePos = 0
    numSentences = 0

    while example['id'] == postId:
        if (example['sid'] == 0):
            sentencePos += 1
            break
        example = examples[example['sid'] - 1]
        sentencePos += 1

    while len(examples) > example['sid'] + 1 and examples[example['sid'] + 1]['id'] == postId:
        example = examples[example['sid'] + 1]
        numSentences += 1

    if numSentences == 0:
        numSentences = 1

    return sentencePos*1.0/numSentences

# Return the absolute frequency of 5w1h in the sentence
# ["what", "where", "when", "why", "who", "how"]
def containsOneOf_5w1h(example):
    sentence = example["sentence"].lower()
    pattern = re.compile("(?:^|\s)(what|whats|what's|when|where|why|who|how)(?:\s|$)")
    result = pattern.findall(sentence)
    if len(result) > 0:
        return 1
    else:
        return 0

# Returns 1/0 whether contains questionmark or not
def hasQuestionmark(example):
    if "?" in example["sentence"]:
        return 1
    else:
        return 0

# Returns 1/0 whether contains thank or not
def hasThank(example):
    if "thank" in example["sentence"].lower():
        return 1
    else:
        return 0

# Returns 1/0 whether contains ! or not
def hasExclamationMark(example):
    if "!" in example["sentence"].lower():
        return 1
    else:
        return 0

# Returns 1/0 wether contains did not, does not.
def hasPositiveFeedback(example):
    if ("did not" in example["sentence"].lower()) or ("does not" in example["sentence"].lower()):
        return 1
    else:
        return 0

# Returns 1/0 whether contains same, similar.
def hasDuplicateWords(example):
    if ("same" in example["sentence"].lower()) or ("similar" in example["sentence"].lower()):
        return 1
    else:
        return 0

# get SentiStrength sentiment
def sentimentValue(example):
    return rateSentiStrengthSentiment(example['sentence'])[2]

# checks whether current sentence exists in the previous post
def isQuote(example, examples):
    text = getParentPostText(example['reply_to_sid'], examples)
    if example['sentence'] in text:
        return 1
    else:
        return 0

def conversationSim(example, examples):
    postText = getPostText(example, examples)
    conversationText = getConversationText(example, examples)
    return cosineSimilarity(postText, conversationText)

def initSim(example, examples):
    postText = getPostText(example, examples)
    rootPostText = getRootPostText(example, examples)
    return cosineSimilarity(postText, rootPostText)