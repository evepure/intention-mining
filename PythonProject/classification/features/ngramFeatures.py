from nltk import FreqDist
from nltk.util import ngrams
from util.datasetUtil import removePunctuation
import numpy as np


class NGram:
    # Document is a list of tokens (should be lowercased already). Examples is a list of maps that contains documents.
    # MinOccurence - take an n-gram into account if its appearance is more or equal than minOccurence
    def __init__(self, n, examples, minOccurence = 5, examplesValue = "tokens"):
        self.n = n
        self.minOccurence = minOccurence
        self.examplesValue = examplesValue

        fdist = {}
        for example in examples:
            document = removePunctuation(example[self.examplesValue])
            for ngram in ngrams(document, self.n):
                fdist[ngram] = fdist.get(ngram, 0) + 1
        #print("Fdist: ", fdist)
        filteredNGrams = [k_v1 for k_v1 in list(fdist.items()) if k_v1[1] >= self.minOccurence]
        #print("Filtered ngrams: ", filteredNGrams)

        self.ngrams = list([" ".join(k_v[0]) for k_v in filteredNGrams])
        #print("Ngrams: ", self.ngrams)
        print(("NGram: created  %d ngram features." % len(self.ngrams)))

    def getFeatureVectorNames(self):
        return self.ngrams

    def getFeatureVectorValues(self, example):
        values = [0] * len(self.ngrams)

        for ngram in ngrams(removePunctuation(example[self.examplesValue]), self.n):
            try:
                index = self.ngrams.index(" ".join(ngram))
                values[index] += 1
            except:
                pass

        return values

    def getNumpyTrainingData(self, examples, classExamplesValue = "class"):
        examplesFeatures = []
        ys = []

        for example in examples:
            examplesFeatures.append(self.getFeatureVectorValues(example))
            ys.append(example[classExamplesValue])

        return (np.array(examplesFeatures), np.array(ys))