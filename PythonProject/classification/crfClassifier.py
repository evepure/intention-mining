from nltk.tag import CRFTagger
from util.datasetUtil import importDataStandard, getPostExamples, getAllPostIds
import math
from classification.features.bhatiaFeatures import *
from classification.features.features import *
from nltk.corpus import stopwords
import string
from constants import CRF_MODEL
import random

examples = importDataStandard()

# TF IDF calculation
idf = dict([(lemma, 1) for example in examples for lemma in example["lemmas"] if lemma not in stopwords.words('english') and lemma not in string.punctuation])
for example in examples:
    for lemma in set(example["lemmas"]):
        if lemma in idf:
            idf[lemma] += 1
for key in idf.keys():
    idf[key] = math.log(len(examples) / idf[key])

def tfidf(lemma, lemmas):
    tf = lemmas.count(lemma) / len(lemmas)
    return tf * idf[lemma]

def topNtfidfLemmas(example, n):
    lemmas = [lemma for lemma in example["lemmas"] if lemma not in stopwords.words('english') and lemma not in string.punctuation]
    scores =  [(lemma, tfidf(lemma, lemmas)) for lemma in lemmas]
    return sorted(scores, key = lambda x: x[1], reverse = True)[:n]
# END TF IDF calculation

num_pattern = re.compile(r'\d')

# FEATURES generation
def getFeatures(examples, idx):
    example = examples[idx]
    tokens = example['tokens']
    lemmas = example['lemmas']
    sentence = example['sentence']

    feature_list = []

    # Capitalization
    if containtsCapsWord(example):
        feature_list.append('CAPS_WORD')

    # Number
    if sum([re.search(num_pattern, token) is not None for token in tokens]) > 0:
        feature_list.append('HAS_NUM')

    if textUniqueLength(tokens) < 5:
        feature_list.append('SHORT')

    if containsOneOf_5w1h(example) == 1:
        feature_list.append('5W1H')

    if hasQuestionmark(example) == 1:
        feature_list.append("QUESTION")

    if hasThank(example) == 1:
        feature_list.append("THANK")

    if hasExclamationMark(example) == 1:
        feature_list.append("EXCLAMATION")

    if hasPositiveFeedback(example) == 1:
        feature_list.append("POS_FEED")

    if hasDuplicateWords(example) == 1:
        feature_list.append("DUPLIC")

    feature_list.append("SENT=" + str(sentimentValue(example)))

    # TOP TFIDF LEMMAS
    for (lemma, _) in topNtfidfLemmas(example, 5):
        feature_list.append("LEMMA=" + lemma)

    if hasLink(example) == 1:
        feature_list.append("LINK")

    if hasPunctuations(example)[":"] == 1:
        feature_list.append("COLON")

    if hasPunctuations(example)[".."] == 1:
        feature_list.append("DOTS")

    if numberOfEmoticons(example) > 0:
        feature_list.append("EMOTICONS")

    if numberOfUnicodeEmoticons(example) > 0:
        feature_list.append("U_EMOTICONS")

    if numberOfInterjections(example) > 0:
        feature_list.append("INTERJECTIONS")

    if verbPos(example) == -1:
        feature_list.append("NO_VERB")
    elif verbPos(example) == 0:
        feature_list.append("VERB_FIRST")
    elif verbPos(example) == len(example["tokens"]) - 1:
        feature_list.append("VERB_LAST")
    else:
        feature_list.append("VERB_MIDDLE")

    for (modal, f) in zip(modals, modalsFunctionGenerator()):
        if f(example) > 0:
            feature_list.append("MODAL=" + modal)

    if numberOfNegations(example) > 0:
        feature_list.append("NEG")

    if hasFutureVerb(example) == 1:
        feature_list.append("FUTURE")

    if verbTense(example)["hasPastV"] == 1:
        feature_list.append("TENSE=hasPastV")

    if verbTense(example)["hasIngV"] == 1:
        feature_list.append("TENSE=hasIngV")

    if verbTense(example)["hasImperativeV"] == 1:
        feature_list.append("TENSE=hasImperativeV")

    if pronominalCues(example)["has_1stperson_sg"] > 0:
        feature_list.append("PRONOMINAL=has_1stperson_sg")

    if pronominalCues(example)["has_1stperson_pl"] > 0:
        feature_list.append("PRONOMINAL=has_1stperson_pl")

    if pronominalCues(example)["has_2ndperson"] > 0:
        feature_list.append("PRONOMINAL=has_2ndperson")

    if pronominalCues(example)["has_3rdperson"] > 0:
        feature_list.append("PRONOMINAL=has_3rdperson")

    return feature_list

# PREPARE DATA
data = []
for postId in getAllPostIds(examples):
    data.append([(example, str((example['class_1']))) for example in getPostExamples(postId, examples)])

# SPLIT DATA
random.shuffle(data)
n_test_samples = int(0.3 * len(data))
train_data = data[:-n_test_samples]
test_data = data[-n_test_samples:]

# TRAIN & TEST
ct = CRFTagger(feature_func=getFeatures, verbose=True, training_opt={})
ct.train(train_data, CRF_MODEL)
print(ct.evaluate(test_data))

# PRINT SOME TAGGING
print(ct.tag_sents([test_data[0], test_data[1]]))



