import sklearn.preprocessing
from sklearn.feature_selection import SelectFromModel
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn import svm, linear_model, neighbors, ensemble, naive_bayes
import numpy as np
from sklearn.metrics import f1_score, precision_score, recall_score
import warnings
from random import shuffle
from ReliefF import ReliefF
#from sklearn.linear_model import RandomizedLasso
from sklearn.feature_selection import RFE
from sklearn.preprocessing import MinMaxScaler

# Supress warnings regarding F1
warnings.filterwarnings("ignore")

def classDistribution(examples):
    allClasses = [example["class"] for example in examples]
    allClassesSet = set(allClasses)
    classesDict = {}
    for c in allClassesSet:
        classesDict[c] = allClasses.count(c)
    return classesDict

def runTrainTestClassification(features, classes, examples, trainExamplesNum, filterIds = None):
    # Filter
    if filterIds:
        examples = [example for i, example in enumerate(examples) if i not in filterIds]
        features = [feature for i, feature in enumerate(features) if i not in filterIds]
        classes = [cls for i, cls in enumerate(classes) if i not in filterIds]

    #Classes to int mapping
    allClasses = list(set(classes))
    allClasses.sort(reverse=True)
    classes = [allClasses.index(cls) for cls in classes]
    print("Mapping of classes to integers (integer class, original class): {}".format(list(map(lambda x: (allClasses.index(x), x), allClasses))))
    (X, y) = (np.array(features), np.array(list(map(int, classes))))

    # Show class counts
    print("Distribution of classes: {}".format(classDistribution(examples)))
    print(("Dataset shape: {}".format((X.shape, y.shape))))

    X = MinMaxScaler().fit_transform(X)

    print("FSS 'Select from model'")
    svc = svm.LinearSVC(C=0.2, penalty="l1", dual=False)
    model = SelectFromModel(svc)
    X = model.fit_transform(X, y)

    # Scoring
    X_train = X[:trainExamplesNum]
    y_train = y[:trainExamplesNum]
    X_test = X[trainExamplesNum:]
    y_test = y[trainExamplesNum:]

    print(trainExamplesNum)
    print(("Dataset shape: {}".format((X_train.shape, y_train.shape, X_test.shape, y_test.shape))))

    clf = linear_model.LogisticRegression()
    clf = clf.fit(X_train, y_train)
    predicted = clf.predict(X_test)
    trues = y_test

    f1w = sklearn.metrics.f1_score(trues, predicted, average='weighted')
    pw = sklearn.metrics.precision_score(trues, predicted, average='weighted')
    rw = sklearn.metrics.recall_score(trues, predicted, average='weighted')

    print("\tP_weighted: {:.2f}, R_weighted: {:.2f}, F1_weighted: {:.2f}".format(pw, rw, f1w))
