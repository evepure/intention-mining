from sklearn.model_selection import train_test_split
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report
import collections
from sklearn.feature_selection import chi2
from util.datasetUtil import importDataStandard
import math
import numpy


def splitDataset(examples):
    train, test = train_test_split(examples, train_size = 0.8)
    return train, test

def getCleanText(examples):
    clean_reviews = []
    for i in range(0, len(examples)):
        clean_reviews.append(" ".join(examples[i]['lemmas']))
    return clean_reviews


def createBagOfWords(clean_text):
    # Initialize the "CountVectorizer" object, which is scikit-learn's
    # bag of words tool.
    vectorizer = CountVectorizer(analyzer="word", \
                                 tokenizer=None, \
                                 preprocessor=None, \
                                 stop_words=None, \
                                 max_features=5000)

    # fit_transform() does two functions: First, it fits the model
    # and learns the vocabulary; second, it transforms our training data
    # into feature vectors. The input to fit_transform should be a list of
    # strings.
    data_features = vectorizer.fit_transform(clean_text)

    # Numpy arrays are easy to work with, so convert the result to an
    # array
    data_features = data_features.toarray()

    # For each, print the vocabulary word and the number of times it
    # appears in the training set
    print(data_features.shape)
    dist = np.sum(data_features, axis=0)
    vocab = vectorizer.get_feature_names()
    for tag, count in zip(vocab, dist):
        print(count, tag)

    return vectorizer, data_features


def trainRandomForest(train, train_data_features):
    # Initialize a Random Forest classifier with 100 trees
    forest = RandomForestClassifier(n_estimators=100)

    # Fit the forest to the training set, using the bag of words as
    # features and the sentiment labels as the response variable
    #
    # This may take a few minutes to run
    classes = [example["class_1"] for example in train]
    forest = forest.fit(train_data_features, classes)

    return forest

def createBagOfWordsForTest(vectorizer, clean_test_text):
    test_data_features = vectorizer.transform(clean_test_text)
    test_data_features = test_data_features.toarray()
    return test_data_features


def bagOfWordsEvaluation(examples):
    # 1. split dataset
    train, test = splitDataset(examples)
    #TODO !!!!!
    #train = examples

    # 2. get train features
    clean_train_text = getCleanText(train)
    vectorizer, train_data_features = createBagOfWords(clean_train_text)

    # 3. train model
    forest = trainRandomForest(train, train_data_features)

    # 4. get train features
    clean_test_text = getCleanText(test)
    test_data_features = createBagOfWordsForTest(vectorizer, clean_test_text)

    # 5. predict
    results = forest.predict(test_data_features)

    # 6. evaluate
    train_trues = []
    for example in train:
        train_trues.append(example['class_1'])


    trues = []
    for example in test:
        trues.append(example['class_1'])

    print("Test data classes: ", collections.Counter(trues))
    print("Predicted classes: ", collections.Counter(results))

    print((classification_report(trues, results)))

    #Chi2
    X = train_data_features
    res = numpy.asarray(train_trues)
    print("X shape: ", X.shape)

    dist = np.sum(X, axis=0)
    vocab = vectorizer.get_feature_names()
    for count, tag in sorted(zip(dist, vocab), key=lambda x: x[0]):
        print(count, tag)

    chi2score, pval = chi2(X, res)
    from pylab import barh, plot, yticks, show, grid, xlabel, figure
    figure(figsize=(6, 6))
    wscores = list(zip(list(zip(vectorizer.get_feature_names(), [round(x, 3) for x in pval])), chi2score))
    wchi2 = sorted(wscores, key=lambda x: x[1])
    topchi2 = list(zip(*wchi2[-50:]))
    x = list(range(len(topchi2[1])))
    labels = topchi2[0]
    barh(x, topchi2[1], align='center', alpha=.2, color='g')
    plot(topchi2[1], x, '-o', markersize=2, alpha=.8, color='g')
    yticks(x, labels)
    xlabel('$\chi^2$')
    show()

    print(topchi2)


examples = importDataStandard()
bagOfWordsEvaluation(examples)