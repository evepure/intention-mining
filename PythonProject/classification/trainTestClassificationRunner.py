from util.datasetUtil import importDataStandard
from util.swdaImport import importSWDADataset
from util.bhatiaImport import importBhatiaDataset
from classification.trainTestClassification import runTrainTestClassification
from classification.features.featureExtraction import featureExtraction
import constants
import copy

def classDistribution(examples):
  allClasses = [example["class_1"] for example in examples] + [example["class_2"] for example in examples if example["class_2"] != ""] + [example["class_3"] for example in examples if example["class_3"] != ""]
  allClassesSet = set(allClasses)
  classesDict = {}
  for c in allClassesSet:
      classesDict[c] = allClasses.count(c)
  return classesDict

def selectTheLeastRepresentedAsClass(examples):
    classDist = classDistribution(examples)
    for example in examples:
        cls = [v for v in [example["class_1"], example["class_2"], example["class_3"]] if v != ""]
        dict = [(c, classDist.get(c)) for c in cls]
        dict.sort(key=lambda x: x[1], reverse=False)
        example["class"] = dict[0][0]

# MULTICLASS EVALUATION: train=Reddit, test=XXX

featureGroupsEvaluations = [
    #[1],
    #[2],
    #[3],
    #[1,2],
    #[2,3],
    #[1,3],
    [1,2,3]
]

def bhatiaEval():
    #Names: ['Solution - 5', 'Question - 1', 'Further details - 4', 'Junk - 8',
    # 'Clarification - 3', 'Other - -1', 'Positive feedback - 7', 'Repeat question - 2',
    # 'Negative feedback - 6']

    bhatiaMapping = {"2": "DIRECT", "3": "DIRECT", "4": "ASSERT",
                     "5": "ASSERT", "7": "THANK", "6": "COMPLAIN"}
    bhatiaOurClasses = set(bhatiaMapping.values())

    # Our data import
    examplesTrain = importDataStandard()
    selectTheLeastRepresentedAsClass(examplesTrain)

    filterIdsTrain = []
    for i, example in enumerate(examplesTrain):
        if example["class"] not in bhatiaOurClasses:
            filterIdsTrain.append(i)

    trainExampleNum = len(examplesTrain) - len(filterIdsTrain)

    # Bhatia import
    examplesTest = importBhatiaDataset()
    filterIdsTest = []
    for i, example in enumerate(examplesTest):
        example["class"] = example["class_1"]
        if example["class"] not in bhatiaMapping.keys():
            filterIdsTest.append(i+len(examplesTrain))
        else:
            example["class"] = bhatiaMapping.get(example["class"])

    testExampleNum = len(examplesTest) - len(filterIdsTest)
    print("Train examples #:", trainExampleNum, ", test examples #:", testExampleNum)

    examples = examplesTrain + examplesTest
    filterIds = filterIdsTrain + filterIdsTest

    for featureGroupEvaluation in featureGroupsEvaluations:
        print("Doing feature group: {}".format(featureGroupEvaluation))
        features, featureNames, classes = featureExtraction(examples, featureGroups=featureGroupEvaluation)
        runTrainTestClassification(copy.deepcopy(features), copy.deepcopy(classes), copy.deepcopy(examples), copy.deepcopy(trainExampleNum), copy.deepcopy(filterIds))

#print("Bhatia Eval")
#bhatiaEval()

def swdaEval():
    swdaMapping = {"sd": "ASSERT", "^q": "ASSERT", "bf": "ASSERT", "aa": "AGREE", "ny": "AGREE", "na": "AGREE", "bk": "AGREE",
                   "nn": "DISAGREE", "ng": "DISAGREE", "arp_nd": "DISAGREE", "h": "GUESS", "aap_am": "GUESS", "no": "GUESS",
                   "qy": "DIRECT", "qw": "DIRECT", "qy^d": "DIRECT", "^g": "DIRECT", "qw^d": "DIRECT", "qh": "DIRECT",
                   "qo": "DIRECT", "ad": "DIRECT", "oo_cc_co": "ENGAGE", "fa": "APOLOGISE", "ft": "THANK"}
    swdaOurClasses = set(swdaMapping.values())

    # Our data import
    examplesTrain = importDataStandard()
    selectTheLeastRepresentedAsClass(examplesTrain)

    filterIdsTrain = []
    for i, example in enumerate(examplesTrain):
        if example["class"] not in swdaOurClasses:
            filterIdsTrain.append(i)

    trainExampleNum = len(examplesTrain) - len(filterIdsTrain)

    # SWDA import
    examplesTest = importSWDADataset(size = 10000)
    filterIdsTest = []
    for i, example in enumerate(examplesTest):
        example["class"] = example["class_1"]
        if example["class"] not in swdaMapping.keys():
            filterIdsTest.append(i + len(examplesTrain))
        else:
            example["class"] = swdaMapping.get(example["class"])

    testExampleNum = len(examplesTest) - len(filterIdsTest)
    print("Train examples #:", trainExampleNum, ", test examples #:", testExampleNum)

    examples = examplesTrain + examplesTest
    filterIds = filterIdsTrain + filterIdsTest

    for featureGroupEvaluation in featureGroupsEvaluations:
        print("Doing feature group: {}".format(featureGroupEvaluation))
        features, featureNames, classes = featureExtraction(examples, featureGroups=featureGroupEvaluation)
        runTrainTestClassification(copy.deepcopy(features), copy.deepcopy(classes), copy.deepcopy(examples), copy.deepcopy(trainExampleNum), copy.deepcopy(filterIds))

#print("SWDA Eval")
#swdaEval()

def standardValidationEval():
    # Standard data import
    examplesTrain = importDataStandard()
    selectTheLeastRepresentedAsClass(examplesTrain)
    trainExampleNum = len(examplesTrain)

    # Validation data import
    examplesTest = importDataStandard(defaultCorpus=constants.CORPUS_VAL_LOCATION)
    selectTheLeastRepresentedAsClass(examplesTest)

    examples = examplesTrain + examplesTest
    filterIds = []

    for featureGroupEvaluation in featureGroupsEvaluations:
        print("Doing feature group: {}".format(featureGroupEvaluation))
        features, featureNames, classes = featureExtraction(examples, featureGroups=featureGroupEvaluation)
        runTrainTestClassification(copy.deepcopy(features), copy.deepcopy(classes), copy.deepcopy(examples), copy.deepcopy(trainExampleNum), copy.deepcopy(filterIds))

print("Standard validation Eval")
standardValidationEval()