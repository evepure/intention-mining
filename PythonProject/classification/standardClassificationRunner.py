from util.datasetUtil import importDataStandard
from util.swdaImport import importSWDADataset
from util.bhatiaImport import importBhatiaDataset
from classification.standardClassification import runStandardClassification
from classification.features.featureExtraction import featureExtraction
import copy

# Init classes mapping
groupToClass = {
    "Assertives": ["ASSERT", "SUSTAIN", "GUESS", "AGREE", "DISAGREE"],
    "Expressives": ["REJOICE", "COMPLAIN", "WISH", "APOLOGIZE", "THANK", "GREET"],
    "Directives": ["DIRECT", "SUGGEST"],
    "Commissives": ["ENGAGE", "ACCEPT", "REFUSE"],
    "Other": ["OTHER"],
    "TO_OMIT": ["TO_OMIT"],
}
classToGroup = dict([(value, key) for key in groupToClass.keys() for value in groupToClass[key]])

def filterByOccurences(examples, cutOffInPercent):
    allClasses = [example["class"] for example in examples]
    allClassesSet = set(allClasses)
    classesDict = {}
    for c in allClassesSet:
        classesDict[c] = allClasses.count(c)
    examplesSize = len(examples)
    return set([i for i, example in enumerate(examples) if classesDict[example["class"]] > cutOffInPercent/100.0*examplesSize])

def getAllClasses(examples):
    cls1 = [example["class_1"] for example in examples]
    cls2 = [example["class_2"] for example in examples]
    cls3 = [example["class_3"] for example in examples]
    retVal = list(set(cls1 + cls2 + cls3))
    retVal = list(filter(lambda v: v != '', retVal))
    return retVal

def mapExamplesForOneAgainstAll(className, examples):
    for example in examples:
        if (example["class_1"] == className or example["class_2"] == className or example["class_3"] == className):
            example["class"] = className
        else:
            example["class"] = "ZZZ"

def classDistribution(examples):
  allClasses = [example["class_1"] for example in examples] + [example["class_2"] for example in examples if example["class_2"] != ""] + [example["class_3"] for example in examples if example["class_3"] != ""]
  allClassesSet = set(allClasses)
  classesDict = {}
  for c in allClassesSet:
      classesDict[c] = allClasses.count(c)
  return classesDict

def selectTheLeastRepresentedAsClass(examples):
    for example in examples:
        if not "class_2" in example:
            example["class_2"] = ""
        if not "class_3" in example:
            example["class_3"] = ""
    classDist = classDistribution(examples)
    for example in examples:
        cls = [v for v in [example["class_1"], example["class_2"], example["class_3"]] if v != ""]
        dict = [(c, classDist.get(c)) for c in cls]
        dict.sort(key=lambda x: x[1], reverse=False)
        example["class"] = dict[0][0]

#########



def evaluateGroups(dataName):
    print("***************\nEVALUATING GROUPS\n***************")
    examples = importData(dataName)

    selectTheLeastRepresentedAsClass(examples)

    for example in examples:
        example["class"] = classToGroup[example["class"]]

    return examples

def evaluateClasses(dataName):
    print("***************\nEVALUATING CLASSES\n***************")
    examples = importData(dataName)

    if (dataName == "STANDARD" or dataName == "STANDARD_EWDC_COMPLAIN" or dataName == "STANDARD_EWDC_ENGAGE"):
        selectTheLeastRepresentedAsClass(examples)
    else:
        for example in examples:
            example["class"] = example["class_1"]

    return examples



def evaluate(evaluation, examples, FSSs, featureGroupsEvaluations):
    for featureGroupEvaluation in featureGroupsEvaluations:
        print("Doing feature group: {}".format(featureGroupEvaluation))
        features, featureNames, classes = featureExtraction(examples, featureGroups=featureGroupEvaluation)
        bhatiaData = None
        swdaData = None
        if RUN_BEST_ON_BHATIA:
            bhatiaData = featureExtraction(evaluateClasses("BHATIA"), featureGroups=featureGroupEvaluation, usePreloadedFeatureKeys=True,
                                           classesToCreateFeatures=['ASSERT', 'DIRECT', 'REJOICE', 'COMPLAIN', "Assertives", "Expressives", "Directives", "Commissives", "Other"])
        if RUN_BEST_ON_SWDA:
            swdaData = featureExtraction(evaluateClasses("SWDA"), featureGroups=featureGroupEvaluation, usePreloadedFeatureKeys=True,
                                           classesToCreateFeatures=['ASSERT', 'DIRECT', 'AGREE', 'GUESS', 'ENGAGE', 'THANK', 'APOLOGIZE', "Assertives", "Expressives", "Directives", "Commissives", "Other"])

        for normalize in [True]:
            for performFSS in FSSs:
                for balance in [False]:
                    print("FSS: {}, Balance: {}, Normalize: {}".format(performFSS, balance, normalize))
                    runStandardClassification(copy.deepcopy(features), copy.deepcopy(featureNames),
                                              copy.deepcopy(classes),
                                              performFSS=performFSS, balance=balance,
                                              normalize=normalize,
                                              bhatiaData=bhatiaData, swdaData=swdaData)


def importData(dataName):
    if (dataName == "STANDARD"):
        return importDataStandard()
    if (dataName == "STANDARD_EWDC_COMPLAIN"):
        return importDataStandard(addEWDCComplain=True)
    if (dataName == "STANDARD_EWDC_ENGAGE"):
        return importDataStandard(addEWDCEngage=True)
    if (dataName == "BHATIA"):
        return importBhatiaDataset()
    if (dataName == "SWDA"):
        return importSWDADataset()
    return -1

# MULTICLASS EVALUATION
#
featureGroupsEvaluations = [
    [1],
    [2],
    [3],
    [1,2],
    [2,3],
    [1,3],
    [1,2,3]
]

fss = [
    0, # No FSS performed, for other options see classification code ...
    #1#,
    #2,
    #3,
    #4
]

dataNames = [
    #"STANDARD_EWDC_COMPLAIN" # OneVsAll setting, also just LogReg algorithm set.
    #"STANDARD_EWDC_ENGAGE" # OneVsAll setting, also just LogReg algorithm set.
    "STANDARD"
    #"BHATIA"
    #"SWDA"
]

# Standard + BHATIA/SWDA
#Separately define groups (here and above update!) and classes due to later examples loading ...!!!!
RUN_BEST_ON_BHATIA = False
RUN_BEST_ON_SWDA = False


for dataName in dataNames:
    print("DOING DATA: " + dataName)
    evaluations = [evaluateClasses]
    if (dataName == "STANDARD"):
        evaluations.append(evaluateGroups)
    #evaluations = [evaluateClasses]

    for evaluation in evaluations:
        examples = evaluation(dataName)
        evaluate(evaluation, examples, fss, featureGroupsEvaluations)
