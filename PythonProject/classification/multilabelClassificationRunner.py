from util.datasetUtil import importDataStandard
from util.swdaImport import importSWDADataset
from util.bhatiaImport import importBhatiaDataset
from classification.multilabelClassification import runMultiLabelClassification
from classification.features.featureExtraction import featureExtraction
import copy

# Init classes mapping
groupToClass = {
    "Assertives": ["ASSERT", "SUSTAIN", "GUESS", "AGREE", "DISAGREE"],
    "Expressives": ["REJOICE", "COMPLAIN", "WISH", "APOLOGIZE", "THANK", "GREET"],
    "Directives": ["DIRECT", "SUGGEST"],
    "Commissives": ["ENGAGE", "ACCEPT", "REFUSE"],
    "Other": ["OTHER"]
}
classToGroup = dict([(value, key) for key in groupToClass.keys() for value in groupToClass[key]])

def filterByOccurences(examples, cutOffInPercent):
    allClasses = [example["class"] for example in examples]
    allClassesSet = set(allClasses)
    classesDict = {}
    for c in allClassesSet:
        classesDict[c] = allClasses.count(c)
    examplesSize = len(examples)
    return set([i for i, example in enumerate(examples) if classesDict[example["class"]] > cutOffInPercent/100.0*examplesSize])

#########



def evaluateGroups():
    print("***************\nEVALUATING GROUPS\n***************")
    examples = importData()
    for example in examples:
        example["class"] = classToGroup[example["class_1"]]
    return (examples, None)

def evaluateClasses():
    print("***************\nEVALUATING CLASSES\n***************")
    examples = importData()
    for example in examples:
        example["class"] = example["class_1"]
    return (examples, None)

def evaluateSingleClasses():
    print("***************\nEVALUATING SINGLE CLASSES\n***************")
    examples = importData()
    for example in examples:
        example["class"] = example["class_1"]

    filterIds = set([i for i, example in enumerate(examples) if example["class_2"] == "" and example["class_3"] == ""])
    return (examples, filterIds)

def evaluateClassesCutOff():
    print("***************\nEVALUATING CLASSES (count > 5%)\n***************")
    examples = importData()
    for example in examples:
        example["class"] = example["class_1"]

    filterIds = filterByOccurences(examples, 5)
    return (examples, filterIds)

def evaluateSingleClassesCutOff():
    print("***************\nEVALUATING SINGLE CLASSES (count > 5%)\n***************")
    examples = importData()
    for example in examples:
        example["class"] = example["class_1"]

    filterIds = filterByOccurences(examples, 5)
    filterIds = filterIds.intersection(set( [i for i, example in enumerate(examples) if example["class_2"] == "" and example["class_3"] == ""] ))
    return (examples, filterIds)

def evaluateSingleClassesNoAssert():
    print("***************\nEVALUATING SINGLE CLASSES NO ASSERT\n***************")
    examples = importData()
    for example in examples:
        example["class"] = example["class_1"]
    filterIds = set([i for i, example in enumerate(examples) if example["class_2"] == "" and example["class_3"] == "" and example["class_1"] != "ASSERT"])
    return (examples, filterIds)

def evaluateClassesCutOffNoAssert():
    print("***************\nEVALUATING CLASSES (count > 5%) NO ASSERT\n***************")
    examples = importData()
    for example in examples:
        example["class"] = example["class_1"]

    filterIds = filterByOccurences(examples, 5)
    filterIds = filterIds.intersection(set( [i for i, example in enumerate(examples) if example["class_1"] != "ASSERT"] ))
    return (examples, filterIds)



def importData():
    return importDataStandard()

evaluations = [
    evaluateClasses
]

featureGroupsEvaluations = [
    [1],
    [2],
    [3],
    [1, 2],
    [2, 3],
    [1, 3],
    [1, 2, 3]
]

for evaluation in evaluations:
    examples, filterIds = evaluation()
    examples = examples

    for featureGroupEvaluation in featureGroupsEvaluations:
        print("Doing feature group: {}".format(featureGroupEvaluation))
        features, featureNames, classes = featureExtraction(examples, featureGroups = featureGroupEvaluation)

        for normalize in [True]:
            print("Normalize: {}".format(normalize))
            runMultiLabelClassification(copy.deepcopy(examples), copy.deepcopy(features), copy.deepcopy(featureNames),
                                        filterIds = filterIds, normalize = normalize)