from classification.features.ngramFeatures import NGram
from sklearn.feature_selection import chi2, SelectKBest
import numpy as np

from util.datasetUtil import importDataStandard

##
## This is only a helper script that calculates ngram features. They are copied and used directly by precalculatedNGramFeatures.py
##


def topNFeatureExtraction(ngram, examples, verbose = False, n=30):
    # prepare features
    features = []
    featureNames = []
    classes = []


    for i in range(0, len(examples)):
        example = examples[i]
        item = []

        item += ngram.getFeatureVectorValues(example)

        features.append(item)
        classes.append(example['class'])

        if verbose and i % 100 == 0:
            print(("Done {} examples.".format(i)))


    featureNames += [name for name in ngram.getFeatureVectorNames()]
    # Check if length match
    assert len(featureNames) == len(features[0])

    y = np.array(classes)
    X = np.array(features)

    selector = SelectKBest(chi2, k=n)
    selector.fit(X, y)

    selectedNGrams = []
    for i in selector.get_support(indices=True):
        selectedNGrams.append(featureNames[i])

    assert len(selectedNGrams) <= n

    print(selectedNGrams)


#Test
if __name__ == "__main__":
    def mapExamplesForOneAgainstAll(className, examples):
        for example in examples:
            if (example["class_1"] == className or example["class_2"] == className or example["class_3"] == className):
                example["class"] = className
            else:
                example["class"] = "ZZZ"


    def getAllClasses(examples):
        cls1 = [example["class_1"] for example in examples]
        cls2 = [example["class_2"] for example in examples]
        cls3 = [example["class_3"] for example in examples]
        return list(set(cls1 + cls2 + cls3))

    examples = importDataStandard()

    ngrams = [
        ("NGRAM_1", NGram(1, examples, minOccurence=1)),
        ("NGRAM_2", NGram(2, examples, minOccurence=1)),
        ("NGRAM_3", NGram(3, examples, minOccurence=1)),
        ("NGRAM_POS_1", NGram(1, examples, minOccurence=1, examplesValue="pos")),
        ("NGRAM_POS_2", NGram(2, examples, minOccurence=1, examplesValue="pos")),
        ("NGRAM_POS_3", NGram(3, examples, minOccurence=1, examplesValue="pos"))
    ]

    for ngramName, ngram in ngrams:
        print("DOING NGRAM " + ngramName + ":")
        for className in getAllClasses(examples):
            print("DOING " + className + ":")
            mapExamplesForOneAgainstAll(className, examples)
            topNFeatureExtraction(ngram, examples)


