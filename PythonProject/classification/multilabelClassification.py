import sklearn.preprocessing
from sklearn.feature_selection import SelectFromModel
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn import svm, linear_model, neighbors, ensemble, naive_bayes
import numpy as np
from sklearn.metrics import f1_score, precision_score, recall_score, accuracy_score, jaccard_similarity_score
import warnings
from random import shuffle
from ReliefF import ReliefF
#from sklearn.linear_model import RandomizedLasso
from sklearn.feature_selection import RFE
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.metrics import hamming_loss
from classification.helpers.hamming import hamming_score
from sklearn.multiclass import OneVsRestClassifier
from sklearn.model_selection import GridSearchCV

from imblearn.over_sampling import SMOTE, RandomOverSampler

# Supress warnings regarding F1
warnings.filterwarnings("ignore")

def classDistribution(examples):
    allClasses = [example["class"] for example in examples]
    allClassesSet = set(allClasses)
    classesDict = {}
    for c in allClassesSet:
        classesDict[c] = allClasses.count(c)
    return classesDict

def runMultiLabelClassification(examples, features, featureNames, filterIds = None, normalize = False):
    # Filter
    if filterIds:
        examples = [example for i,example in enumerate(examples) if i in filterIds]
        features = [feature for i, feature in enumerate(features) if i in filterIds]

    #Classes to int mapping and binarization
    classes = [[example["class_1"], example["class_2"], example["class_3"]] for example in examples]
    allClasses = []
    for i in range(len(classes)):
        classes[i] = list(filter(lambda x: x != "", classes[i]))
        allClasses += classes[i]
    allClasses = list(set(allClasses))
    allClasses.sort()

    mlb = MultiLabelBinarizer(classes=allClasses)
    y = mlb.fit_transform(classes)
    X = np.array(features)

    # Show class counts
    print("Distribution of classes: {}".format(classDistribution(examples)))
    print(("Dataset shape: {}".format((X.shape, y.shape))))

    # Calculate class pairs
    classPairs = []
    #count2 = 0
    #count3 = 0
    for excls in classes:
        exclsSorted = sorted(excls)
        if len(exclsSorted) == 2:
            classPairs.append((exclsSorted[0], exclsSorted[1]))
        elif len(exclsSorted) == 3:
            classPairs.append((exclsSorted[0], exclsSorted[1]))
            classPairs.append((exclsSorted[0], exclsSorted[2]))
            classPairs.append((exclsSorted[1], exclsSorted[2]))
        elif len(exclsSorted) != 1:
            print("Something went fishy!")
            exit(-1)
    classPairs = list(set(classPairs))
    #print("The dataset has '{}' examples with 2 classes and '{}' examples with 3 classes.".format(count2, count3))

    # Normalize/scaling
    if (normalize):
        X = MinMaxScaler().fit_transform(X)

    # Scoring cross-validation
    print("Scoring: ")
    clfs = [
        #OneVsRestClassifier(linear_model.LogisticRegression())

        GridSearchCV(
            OneVsRestClassifier(linear_model.LogisticRegression()),
            [{'estimator__C': list(np.arange(0.5, 4.5, 0.2)), 'estimator__max_iter': list(range(1,10, 1))}],
            cv=3, scoring="f1_weighted")
    ]
    X_train, X_test, y_train, y_true = train_test_split(X, y, test_size=0.33, random_state=42)

    for clf in clfs:
        print(("{}:".format(clf.__class__.__name__)))

        clf = clf.fit(X_train, y_train)
        y_pred = clf.predict(X_test)

        print("\tH score: {:.1f}, H loss: {:.1f}, A score: {:.1f}, F1 samples: {:.1f}, F1 weighted: {:.1f}, F1 micro: {:.1f}, F1 macro: {:.1f}, Jacc: {:.1f}".format(
            hamming_score(y_true, y_pred)*100,
            hamming_loss(y_true, y_pred)*100,
            accuracy_score(y_true, y_pred, normalize=True, sample_weight=None)*100,
            f1_score(y_true, y_pred, average='samples')*100,
            f1_score(y_true, y_pred, average='weighted')*100,
            f1_score(y_true, y_pred, average='micro') * 100,
            f1_score(y_true, y_pred, average='macro') * 100,
            jaccard_similarity_score(y_true, y_pred)*100))

        for cls in allClasses:
            clsIdx = allClasses.index(cls)
            print("\tF score for class '{}' : weighted: {:.1f}, micro: {:.1f}, macro: {:.1f}".format(
                cls,
                f1_score(y_true, y_pred, average='weighted', labels=[clsIdx]) * 100,
                f1_score(y_true, y_pred, average='micro', labels=[clsIdx]) * 100,
                f1_score(y_true, y_pred, average='macro', labels=[clsIdx]) * 100))

        for pair in classPairs:
            print("\tF score for classes '{}' and '{}' : weighted: {:.1f}, micro: {:.1f}, macro: {:.1f}".format(
                pair[0],
                pair[1],
                f1_score(y_true, y_pred, average='weighted', labels=[allClasses.index(pair[0]), allClasses.index(pair[1])]) * 100,
                f1_score(y_true, y_pred, average='micro', labels=[allClasses.index(pair[0]), allClasses.index(pair[1])]) * 100,
                f1_score(y_true, y_pred, average='macro', labels=[allClasses.index(pair[0]), allClasses.index(pair[1])]) * 100))



