import sklearn.preprocessing
from sklearn.feature_selection import SelectFromModel
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.model_selection import StratifiedShuffleSplit
from sklearn import svm, linear_model, neighbors, ensemble, naive_bayes
import numpy as np
from sklearn.metrics import f1_score, precision_score, recall_score, cohen_kappa_score, confusion_matrix
import warnings
from random import shuffle
from ReliefF import ReliefF
#from sklearn.linear_model import RandomizedLasso
from sklearn.feature_selection import RFE
from sklearn.preprocessing import MinMaxScaler
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import StratifiedKFold

from imblearn.pipeline import make_pipeline as make_pipeline_imb
from imblearn.combine import SMOTETomek
from imblearn.over_sampling import SMOTE, RandomOverSampler
import matplotlib.pyplot as plt
import itertools

import scipy.stats as stats

# Supress warnings regarding F1
warnings.filterwarnings("ignore")

def classDistribution(classes):
    allClasses = classes
    allClassesSet = set(allClasses)
    classesDict = {}
    for c in allClassesSet:
        classesDict[c] = allClasses.count(c)
    return classesDict

def performFSSOnData(featureNames, performFSS,X, y, X_test):
    sortedFeatures = None
    # Features selection options:
    #  - http://blog.datadive.net/selecting-good-features-part-iv-stability-selection-rfe-and-everything-side-by-side/
    # 1 = SELECT FROM MODEL
    if (performFSS == 1):
        #print("FSS 'Select from model'")
        svc = svm.LinearSVC(C=0.2, penalty="l1", dual=False)
        model = SelectFromModel(svc)
        X = model.fit_transform(X, y)
        X_test = model.transform(X_test)
        # print("Features NOT sorted by their score (all selected features):")
        # print(list(filter(lambda x: x[0], zip(model.get_support(indices=False), featureNames))))
        sortedFeatures = list(filter(lambda x: x[0], zip(model.get_support(indices=False), featureNames)))

    # 2 = RELIEFF
    if (performFSS == 2):
        #print("FSS 'ReliefF'")
        fs = ReliefF(n_neighbors=50, n_features_to_keep=50)
        X = fs.fit_transform(X, y)
        X_test = fs.transform(X_test)
        #print("Features sorted by their score:")
        #print(sorted(zip(map(lambda x: round(x, 4), fs.feature_scores), featureNames), reverse=True)[:50])
        sortedFeatures = sorted(zip(map(lambda x: round(x, 4), fs.feature_scores), featureNames), reverse=True)[:50]

    # 3 = RandomizedLasso
    #if (performFSS == 3):
    #    print("FSS 'RandomizedLasso'")
    #    rlasso = RandomizedLasso(selection_threshold=0.01)
    #    X = rlasso.fit_transform(X, y)
    #    X_test = rlasso.transform(X_test)
        # print("Features sorted by their score:")
        # print(sorted(zip(map(lambda x: round(x, 4), rlasso.scores_), featureNames), reverse=True)[:100])

    # 4 = RFE
    if (performFSS == 4):
        print("FSS 'RFE'")
        lr = linear_model.LogisticRegression()
        rfe = RFE(lr)
        X = rfe.fit_transform(X, y)
        X_test = rfe.transform(X_test)
        # print("Features sorted by their score:")
        # print(sorted(zip(map(lambda x: round(x, 4), rfe.ranking_), featureNames), reverse=True)[:100])

    return (X, X_test, sortedFeatures)


###
### Perform FSS:
###     0 - no FSS
###     1 - SelectFromModel (SVC)
###     2 - ReliefF
###     3 - RandomizedLasso
###     4 - RFE
###
def runStandardClassification(features, featureNames, classes, performFSS = 0,
                              balance = False, normalize = False, bhatiaData = None, swdaData = None):

    #Classes to int mapping
    print("Class distribution: {}".format(classDistribution(classes)))
    allClasses = list(set(classes))
    allClasses.sort(reverse=True)
    allClassesIdxs = list(set([allClasses.index(cls) for cls in allClasses]))
    allClassesIdxs.sort()
    print("Sorted classes: {}".format(allClassesIdxs))
    classes = [allClasses.index(cls) for cls in classes]
    print("Mapping of classes to integers (integer class, original class): {}".format(list(map(lambda x: (allClasses.index(x), x), allClasses))))
    (X, y) = (np.array(features), np.array(list(map(int, classes))))

    # Show class counts
    #print("Distribution of classes: {}".format(classDistribution(examples)))
    print(("Dataset shape: {}".format((X.shape, y.shape))))

    # Normalize/scaling
    if (normalize):
        normalizer = MinMaxScaler()
        normalizer.fit(X)
        X = normalizer.transform(X)

    if  bhatiaData:
        #Map classes
        featuresBhatia, featureNamesBhatia, classesBhatia = bhatiaData
        classesBhatia = [allClasses.index(cls) for cls in classesBhatia]
        (X_bhatia, y_bhatia) = (np.array(featuresBhatia), np.array(list(map(int, classesBhatia))))
        #Normalize
        if normalize:
            X_bhatia = normalizer.transform(X_bhatia)

    if swdaData:
        # Map classes
        featuresSWDA, featureNamesSWDA, classesSWDA = swdaData
        classesSWDA = [allClasses.index(cls) for cls in classesSWDA]
        (X_SWDA, y_SWDA) = (np.array(featuresSWDA), np.array(list(map(int, classesSWDA))))
        # Normalize
        if normalize:
            X_SWDA = normalizer.transform(X_SWDA)

    # Scoring cross-validation
    print("Cross-validation scoring: ")
    cv = 10

    clfs = [
        GridSearchCV(
            linear_model.LogisticRegression(),
            [{'C': list(np.arange(0.5, 4.5, 0.2)), 'max_iter': list(range(1, 10, 2))}],
            cv=3, scoring="f1_macro", n_jobs=1),

        GridSearchCV(
            svm.LinearSVC(),
            [{'C': list(np.arange(0.5, 4.5, 0.2))}],
            cv=3, scoring="f1_macro", n_jobs=1),

        GridSearchCV(
            ensemble.RandomForestClassifier(),
            [{'n_estimators': list(np.arange(10, 30, 2))}],
            cv=3, scoring="f1_macro", n_jobs=1)
    ]

    allPredictions = []
    for clf in clfs:
        clf_predictions = []
        print(("{}:".format(clf.estimator.__class__.__name__)))

        # Doing each fold separately
        folding = StratifiedKFold(n_splits=cv, random_state=42)
        bestF = 0.
        bestP = 0.
        bestR = 0.
        bestParams = None
        bestCohenKappa = 0
        best_cnf_matrix = None
        best_estimator = None
        bestFeatures = None
        for k, (train, test) in enumerate(folding.split(X, y)):
            (X_train, y_train, X_test, y_test) = (X[train], y[train], X[test], y[test])

            if (balance):
                bclf = RandomOverSampler(random_state=42)
                X_train, y_train = bclf.fit_sample(X_train, y_train)

            (X_train, X_test, sortedFeatures) = performFSSOnData(featureNames, performFSS, X_train, y_train, X_test)

            clf.fit(X_train, y_train)

            # Update best scores
            y_pred = clf.best_estimator_.predict(X_test)
            clf_predictions += list(y_pred)
            P = precision_score(y_test, y_pred, average='macro') * 100
            R = recall_score(y_test, y_pred, average='macro') * 100
            F1 = f1_score(y_test, y_pred, average='macro') * 100
            if (F1 > bestF):
                bestP = P
                bestR = R
                bestF = F1
                bestParams = clf.best_params_
                bestCohenKappa = cohen_kappa_score(y_test, y_pred)
                best_cnf_matrix = confusion_matrix(y_test, y_pred, labels=allClassesIdxs)
                best_estimator = clf.best_estimator_
                bestFeatures = sortedFeatures

        # Output best scores
        print("\tBest features: {}".format(bestFeatures))
        print("\tBest: Pmacro: {:.1f}, Rmacro: {:.1f}, F1macro: {:.1f}, Cohen's Kappa: {:.2f}, params: {}".format(bestP, bestR, bestF, bestCohenKappa, bestParams))
        print("\tCNF matrix: {}".format(best_cnf_matrix))
        allPredictions.append(clf_predictions)

        if bhatiaData:
            y_pred_bhatia = best_estimator.predict(X_bhatia)
            P = precision_score(y_bhatia, y_pred_bhatia, average='macro') * 100
            R = recall_score(y_bhatia, y_pred_bhatia, average='macro') * 100
            F1 = f1_score(y_bhatia, y_pred_bhatia, average='macro') * 100
            cohenKappa = cohen_kappa_score(y_bhatia, y_pred_bhatia)
            print("\tBHATIA results:")
            print("\t\tPmacro: {:.1f}, Rmacro: {:.1f}, F1macro: {:.1f}, Cohen's Kappa: {:.2f}".format(P, R, F1, cohenKappa))

        if swdaData:
            y_pred_SWDA = best_estimator.predict(X_SWDA)
            P = precision_score(y_SWDA, y_pred_SWDA, average='macro') * 100
            R = recall_score(y_SWDA, y_pred_SWDA, average='macro') * 100
            F1 = f1_score(y_SWDA, y_pred_SWDA, average='macro') * 100
            cohenKappa = cohen_kappa_score(y_SWDA, y_pred_SWDA)
            print("\tSWDA results:")
            print("\t\tPmacro: {:.1f}, Rmacro: {:.1f}, F1macro: {:.1f}, Cohen's Kappa: {:.2f}".format(P, R, F1, cohenKappa))

    # Compare algorithms:
    print("Algorithms comparison: ")
    # 1 and 2
    print("\tT for LG and SVC t,p: {}".format(stats.ttest_rel(allPredictions[0], allPredictions[1])))
    print("\tW for LG and SVC t,p: {}".format(stats.wilcoxon(allPredictions[0], allPredictions[1])))
    print("\tK for LG and SVC: {}".format(cohen_kappa_score(allPredictions[0], allPredictions[1])))
    # 2 and 3
    print("\tT for SVC and RF t,p: {}".format(stats.ttest_rel(allPredictions[1], allPredictions[2])))
    print("\tW for SVC and RF t,p: {}".format(stats.wilcoxon(allPredictions[1], allPredictions[2])))
    print("\tK for SVC and RF: {}".format(cohen_kappa_score(allPredictions[1], allPredictions[2])))
    # 1 and 3
    print("\tT for LG and RF t,p: {}".format(stats.ttest_rel(allPredictions[0], allPredictions[2])))
    print("\tW for LG and RF t,p: {}".format(stats.wilcoxon(allPredictions[0], allPredictions[2])))
    print("\tK for LG and RF: {}".format(cohen_kappa_score(allPredictions[0], allPredictions[2])))

