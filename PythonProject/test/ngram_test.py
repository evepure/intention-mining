from classification.features.ngramFeatures import NGram

dataset = [
    {"tokens": ["i", "am", "a", "good", "boy"]},
    {"tokens": ["good", "boy", "i", "am"]},
    {"tokens": ["good", "boy", "does", "good", "things"]}
]

ngram = NGram(2, dataset, minOccurence=2)

assert ngram.getFeatureVectorValues(dataset[0]) == [1, 1]
assert ngram.getFeatureVectorValues(dataset[1]) == [1, 1]
assert ngram.getFeatureVectorValues(dataset[2]) == [1, 0]