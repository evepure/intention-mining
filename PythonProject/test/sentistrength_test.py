from util.SentiStrength.SentiStrength import rateSentiStrengthSentiment

text = "I am walking down the street"
print(rateSentiStrengthSentiment(text))

text = "I am good boy"
print(rateSentiStrengthSentiment(text))

text = "I am bad boy"
print(rateSentiStrengthSentiment(text))

text = "I hate you!"
print(rateSentiStrengthSentiment(text))

text = "I love you!"
print(rateSentiStrengthSentiment(text))

text = "I love you but hate the current political climate."
print(rateSentiStrengthSentiment(text))