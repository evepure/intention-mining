#!/usr/bin/env python
# -*- coding: utf-8 -*-

import csv

from classification import features
from preprocessing import preprocessing, dataCleaning
from util.datasetUtil import importDataStandard
from classification.features.bhatiaFeatures import sentencePos, sentenceNormPos
from classification.features.features import *

########################
##### Examples init ####
########################

#Import data
examples = importDataStandard()

# postPos
#assert features.postPos(examples[985], examples) == 1
#assert features.postPos(examples[975], examples) == 4
#assert features.postPos(examples[974], examples) == 4
#assert features.postPos(examples[955], examples) == 0
#assert features.postPos(examples[27], examples) == 2

# sentencePos
#assert sentencePos(examples[985], examples) == 2
#assert sentencePos(examples[975], examples) == 1
#assert sentencePos(examples[974], examples) == 13
#assert sentencePos(examples[955], examples) == 5
#assert sentencePos(examples[27], examples) == 15

#print sentenceNormPos(examples[985], examples)
#print sentenceNormPos(examples[975], examples)
#print sentenceNormPos(examples[974], examples)
#print sentenceNormPos(examples[955], examples)
#print sentenceNormPos(examples[27], examples)


# numberOfEmoticons
#assert features.numberOfEmoticons(examples[39]) == 1
#assert features.numberOfEmoticons(examples[70]) == 1
#assert features.numberOfEmoticons(examples[201]) == 0
#assert features.numberOfEmoticons(examples[693]) == 1

# numberOfInterjections
#assert features.numberOfInterjections(examples[693]) == 1
#assert features.numberOfInterjections(examples[151]) == 1
#assert features.numberOfInterjections(examples[250]) == 1
#assert features.numberOfInterjections(examples[834]) == 0

#
assert 1 == hasNounBeforeVerb(examples[10])
assert 0 == hasNounBeforeVerb(examples[282])
