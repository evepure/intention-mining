#!/usr/bin/env python
# -*- coding: utf-8 -*-

from classification.features.bhatiaFeatures import *


sent1 = "http://t.co/NtXDfIHOKa :) :D being an did not athlete with an incurable autoimmune disorder myself this is so inspiring 😌?"
sent2 = '#BuenViernes  "la vida es un espectáculo imperdible" 🌻☀️💦@beatrizarangor'
assert hasPositiveFeedback({"sentence": sent1}) == 1
assert hasPositiveFeedback({"sentence": sent2}) == 0
