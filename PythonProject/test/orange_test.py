import csv

from classification.features.features import *
from classification.features.bhatiaFeatures import *
from preprocessing import preprocessing, dataCleaning
from util.datasetUtil import getParentPostText, importDataStandard
from util.bhatiaImport import importBhatiaDataset

########################
##### MAIN PROGRAM #####
########################

#Import data
#examples = importDataStandard()
#outputDataset = '../../Data/dataset.tab'

examples = importBhatiaDataset()
outputDataset = '../../Data/dataset_bhatia.tab'



dataset = []

##########################
## CREATING THE DATASET ##
## #######################

# First line: attribute names
punctuationKeys = list(hasPunctuations(examples[0]).keys())
verbTenseKeys = list(verbTense(examples[0]).keys())
pronominalKeys = list(pronominalCues(examples[0]).keys())
statLiu = list(statisticsOpinionWordsLiu(examples[0]).keys())
statWilson = list(statisticsOpinionWordsWilson(examples[0], weakIncluded=False).keys())

attrNames = ["hasLink"] + punctuationKeys + ["frequency5w1h", "postPos", "sentencePos", "numberOfEmoticons", \
             "numberOfUnicodeEmoticons", "numberOfInterjections", "cosineSim", "containtsCapsWord", "verbPos", "numberOfNegations", \
             "hasFutureVerb"] + verbTenseKeys + pronominalKeys + ["textLength", "textUniqueLength", "textStemmedLength", "textStemmedUniqueLength"] \
             + statLiu + statWilson + \
             ["can", "cannot", "can't", "cant", "could", "couldn't", \
             "couldnt", "must", "mustn't", "mustnt", "might", "mightn't", "mightnt", "should", "shouldn't", "shouldnt", \
             "may", "mayn't", "maynt", "class"]
dataset.append(attrNames)
# Second line d/c
types = ["c"] * len(attrNames)
for i in [0, 1, 2, 3, 4, 12, 15, 16, 17, 18, len(attrNames)-1]:
    types[i] = "d"
dataset.append(types)
''' Properties - discrete or continous

hasLink                     d
?                           d
!                           d
:                           d
..                          d
frequency5w1h               c
postPos                     c
sentencePos                 c
numberOfEmoticons           c
numberOfUnicodeEmoticons    c
numberOfInterjections       c
cosineSim                   c
containtsCapsWord           d
verbPos                     c
numberOfNegations           c
hasFutureVerb               d
hasPastV                    d
hasIngV                     d
hasImperativeV              d
has_1stperson_sg            c
has_1stperson_pl            c
has_2ndperson               c
has_3rdperson               c
textLength                  c
textUniqueLength            c
textStemmedLength           c
textStemmedUniqueLength     c
numberPosL                  c
numberNegL                  c
ratioPosL                   c
ratioNegL                   c
numberPosW                  c
numberNegW                  c
ratioPosW                   c
ratioNegW                   c
can                         c
cannot                      c
can't                       c
cant                        c
could                       c
couldn't                    c
couldnt                     c
must                        c
mustn't                     c
mustnt                      c
might                       c
mightn't                    c
mightnt                     c
should                      c
shouldn't                   c
shouldnt                    c
may                         c
mayn't                      c
maynt                       c
'''

# Third line class column definition
dataset.append(([""] * (len(attrNames)-1)) + ["class"])

# all other lines: examples
startId = 0
for i in range(startId, len(examples)):
    example = examples[i]
    item = []
    item.append(hasLink(example))
    item = item + [hasPunctuations(example)[key] for key in punctuationKeys]
    item.append(frequency5w1h(example))
    item.append(postPos(example, examples))
    item.append(sentencePos(example, examples))
    item.append(numberOfEmoticons(example))
    item.append(numberOfUnicodeEmoticons(example))
    item.append(numberOfInterjections(example))
    item.append(cosineSim(example['sentence'], getParentPostText(example['reply_to_sid'], examples)))
    item.append(containtsCapsWord(example))
    item.append(verbPos(example))
    item.append(numberOfNegations(example))
    item.append(hasFutureVerb(example))
    item = item + [verbTense(example)[key] for key in verbTenseKeys]
    item = item + [pronominalCues(example)[key] for key in pronominalKeys]
    item.append(textLength(example['tokens']))
    item.append(textUniqueLength(example['tokens']))
    item.append(textStemmedLength(example['tokens']))
    item.append(textStemmedUniqueLength(example['tokens']))
    item = item + [statisticsOpinionWordsLiu(example)[key] for key in statLiu]
    item = item + [statisticsOpinionWordsWilson(example, weakIncluded=False)[key] for key in statWilson]
    for f in modalsFunctionGenerator():
        item.append(f(example))
    item.append(example['class_1'])
    dataset.append(item)
    print("Doing example ", example['sid'])

file = open(outputDataset, 'w')
for example in dataset:
    file.write("\t".join(str(x) for x in example) + "\n")
file.close()