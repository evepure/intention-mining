
from util.datasetUtil import importDataStandard, getPostText, getConversationText, getRootPostText

##########

examples = importDataStandard()

assert getPostText({"id": "cw5369t", "sid": 746}, examples) == " Certainly looks like mine. Same symptoms, too."
assert getPostText({"id": "cw5eym8", "sid": 756}, examples) == " Thanks for the reply. I am on the BC pill and my doctor has been discussing alternative methods with me. After I finish this pack I will likely go off it. I'm afraid of introducing anything new (like depo or an IUD) at this point because I feel like my body will freak out."


assert getConversationText(examples[570], examples) == "I need some support/validation. This is actually a fairly positive post. Even though I am a contractor (at a very big company, where in fact, most of us are, so we don't really get proper sick days...blame corporate America...), I decided to take the day off. I am feeling weirdly guilty, though, because while I'm not in a full blown flare, I just don't feel that well. I've worked through full blown flares before, because I didn't want to take any days off. I've only taken half a day off prior to this, so I'm proud of myself. It's not about the money, but I just feel like I don't look sick enough? Or like I am letting down my team? Or like I don't deserve it? On the other hand, it feels really good. I've been having a lot of joint pain, so I plan to spend the day soaking my joints and running some errands if I am capable (so I don't feel like I'm playing hooky). Not sure what I was going for with this post, but I just wanted to type all that out. Sometimes we just need to have a me day to rebalance. Enjoy your day sounds like you deserve it to me! Thank you, I really appreciate it. I'm feeling a little less guilty already! :)"

assert getRootPostText(examples[570], examples) == getRootPostText(examples[576], examples)