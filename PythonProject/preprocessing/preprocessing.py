from nltk.stem import WordNetLemmatizer
from nltk.tokenize import word_tokenize


def addLemmas(examples):
    wordnet_lemmatizer = WordNetLemmatizer()

    for example in examples:
        lemmas = list(map(wordnet_lemmatizer.lemmatize, example['tokens']))
        example['lemmas'] = lemmas


def addTokens(examples):
    for example in examples:
        tokens = word_tokenize(example['sentence'].lower())
        example['tokens'] = tokens

# Recreate entire posts by concatenating the sentences
# The result is a dictionary {Reddit Id: Text}
def recreatePostsById(examples):
    result = {}
    for example in examples:
        id = example["id"]
        sentence = example["sentence"]

        if id not in result:
            result[id] = []
        result[id].append(sentence)
    return result

# Map the original id on the assigned id
def mapIds(examples):
    result = {}
    for example in examples:
        id = example["id"]
        sid = example["sid"]
        result[sid] = id
    return result