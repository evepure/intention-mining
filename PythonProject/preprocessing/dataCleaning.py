#!/usr/bin/env python
# -*- coding: utf-8 -*-

import re

###############################
### Preetify the dataset ######
###############################

def numberOfPluses(sentence):
    m = re.match("^(\++)", sentence)
    if m:
        return len(m.group(1))
    else:
        return 0

#find the first previous example that has the targetPluses number of pluses
def findReplyToExample(i, targetPluses, examples):
    if (numberOfPluses(examples[i]['sentence']) == targetPluses):
        return i
    else:
        return findReplyToExample(i-1, targetPluses, examples)


def preetify(examples):
    # 1. enumerate examples
    for i, example in enumerate(examples):
        example['sid'] = i
        example['reply_to_sid'] = i
        example['reply_to_id'] = example['id']

    # 2. check pluses and add appropriate 'reply_to_sid'
    for i, example in enumerate(examples):
        curPluses = numberOfPluses(example['sentence'])
        if (curPluses > 0):
            replyToSid = findReplyToExample(i-1, curPluses-1, examples)
            example['reply_to_sid'] = replyToSid
            example['reply_to_id'] = examples[replyToSid]['id']

    # 3. remove starting pluses and trim
    for example in examples:
        example['sentence'] = re.sub("^(\++)", "", example['sentence']).strip()
        example['sentence'] = re.sub("^(\"+)", "", example['sentence']).strip()
        example['sentence'] = re.sub("(\"+)$", "", example['sentence']).strip()

    # 4. clean sentences
    for example in examples:
        example['sentence'] = example['sentence'].replace('&amp;', '&').replace('&gt;', '>').replace('&lt;', '<')
        example['sentence'] = example['sentence'].replace('<b>', '').replace('</b>', '')
        example['sentence'] = example['sentence'].replace('‘', "'").replace('’', "'").replace('“', '"').replace('”', '"')
