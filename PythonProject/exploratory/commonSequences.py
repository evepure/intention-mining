from util.datasetUtil import importDataStandard, getAllPostIds, getPostExamples
import re

examples = importDataStandard()
print("All class types: ", set([example["class_1"] for example in examples]))

allPostIds = getAllPostIds(examples)

sequences = {}

for postId in allPostIds:
    classes = [str(example["class_1"]) for example in getPostExamples(postId, examples)]

    sequence = "".join(classes)
    sequence = re.sub(r"00+", "0*", sequence)
    sequence = re.sub(r"11+", "1*", sequence)
    sequence = re.sub(r"22+", "2*", sequence)
    sequence = re.sub(r"33+", "3*", sequence)
    sequence = re.sub(r"44+", "4*", sequence)


    sequences[sequence] = sequences.get(sequence, 0) + 1

print(sorted(list(sequences.items()), key = lambda x: x[1], reverse = True))

