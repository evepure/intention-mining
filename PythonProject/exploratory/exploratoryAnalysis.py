import csv
from preprocessing import preprocessing, dataCleaning
from util.datasetUtil import getRootPostIds, getLeafPostIds, getConversationPaths, getAllPostIds, importDataStandard
from util.bhatiaImport import importBhatiaDataset
from util.swdaImport import importSWDADataset

########################
##### MAIN PROGRAM #####
########################

#Import data
#examples = importDataStandard(addEWDC=False)
examples = importSWDADataset()
#examples = importBhatiaDataset()

# Print some metrices
print("Examples number (sentences):", len(examples))
print("Tokens number:", sum([len(example['tokens']) for example in examples]))
#print("Leaf or singleton posts:", len(getLeafPostIds(examples)))
#print("Root posts (threads):", len(getRootPostIds(examples)))
#print("Conversation paths:", len(getConversationPaths(examples)))
#print("Conversation paths lengths:", [len(path) for path in getConversationPaths(examples)])
print("Posts number:", len(getAllPostIds(examples)))

# Our dataset only
#print("Examples number:", len(examples))
#print("Only class 1 set:", len(list(filter(lambda x: x['class_1'] != '' and x['class_2'] == '' and x['class_3'] == '', examples))))
#print("Only class 1 and 2 set:", len(list(filter(lambda x: x['class_1'] != '' and x['class_2'] != '' and x['class_3'] == '', examples))))
#print("All three classes set:", len(list(filter(lambda x: x['class_1'] != '' and x['class_2'] != '' and x['class_3'] != '', examples))))

####
#### Prepare Conversation paths output for R script:
####
#lengths = sorted([len(path) for path in getConversationPaths(examples)])
#keys = range(lengths[0], lengths[len(lengths)-1] + 1)
#values = [lengths.count(key) for key in keys]
#print "Keys:", keys
#print "Values:", values

####
#### Prepare distribution data output for R script:
####
def classDistribution(examples):
   allClasses = [example["class_1"] for example in examples]
   allClassesSet = set(allClasses)
   classesDict = {}
   for c in allClassesSet:
       classesDict[c] = allClasses.count(c)
   return classesDict
dict = [(k,v) for k,v in classDistribution(examples).items()]
dict.sort(key=lambda x: x[1], reverse=True)
print("Names:", [k for k,v in dict])
print("Counts", [v for k,v in dict])
#
####
#### Prepare distribution data output for R script (by groups - our dataset only):
####
"""
classToGroup = {'SUGGEST': 'Directives', 'GREET': 'Expressives', 'COMPLAIN': 'Expressives', 'REJOICE': 'Expressives', 'WISH': 'Expressives', 'DIRECT': 'Directives', 'DISAGREE': 'Assertives', 'SUSTAIN': 'Assertives', 'ASSERT': 'Assertives', 'THANK': 'Expressives', 'ACCEPT': 'Commissives', 'AGREE': 'Assertives', 'GUESS': 'Assertives', 'OTHER': 'Other', 'ENGAGE': 'Commissives', 'REFUSE': 'Commissives', 'APOLOGIZE': 'Expressives'}

def classDistribution(examples):
    allClasses = [classToGroup[example["class_1"]] for example in examples]
    allClassesSet = set(allClasses)
    classesDict = {}
    for c in allClassesSet:
        classesDict[c] = allClasses.count(c)
    return classesDict
dict = [(k,v) for k,v in classDistribution(examples).items()]
dict.sort(key=lambda x: x[1], reverse=True)
print("Group distribution:")
print("Names:", [k for k,v in dict])
print("Counts", [v for k,v in dict])
"""