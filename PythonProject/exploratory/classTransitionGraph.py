from itertools import groupby
from util.datasetUtil import importDataStandard

########################
##### MAIN PROGRAM #####
########################

#Import data
examples = importDataStandard()

def mapId(id):
    ids = {0: "Assertive", 1: "Expressive", 2: "Directive", 3: "Commisive", 4: "Other"}
    return ids[id]

#Group sentences to posts.
examplesById = {}
for k, v in groupby(examples, lambda example: example['id']):
    examplesById[k] = list(v)
assert len(examples) == len([item for sublist in list(examplesById.values()) for item in sublist])

transitionCounts = {}
for post in list(examplesById.values()):
    if len(post) >= 2:
        transitions = [(post[i]['class_1'], post[i+1]['class_1']) for i in range(len(post)-1)]
        for transition in transitions:
            transitionCounts[transition] = transitionCounts.get(transition, 0) + 1

#Transitions:
# (a,b,c) = transition from a to b was seen c times
transitions = sorted([(k,v) for k,v in transitionCounts.items()], key = lambda k_v: k_v[1], reverse = True)
print(transitions)

from graphviz import Digraph
graph = Digraph(comment='Transitions')

for n in range(5):
    graph.node(mapId(n))

maxCount = max([w for (_, w) in transitions])
sumCount = sum([w for (_, w) in transitions])
dampingFactor = 10.0
for ((a,b), count) in transitions:
    graph.edge(mapId(a), mapId(b), penwidth = str(dampingFactor*count/maxCount), label = "   " + str(round(count/sumCount*100, 3)) + "%")

graph.render('../img/class1_transition_graph.gv', view=True)
