Bhatia Eval
Train examples #: 1489 , test examples #: 344
Doing feature group: [1, 2, 3]
NGram: created  2178 ngram features.
NGram: created  3455 ngram features.
NGram: created  1294 ngram features.
Done 0 examples.
Done 100 examples.
Done 200 examples.
Done 300 examples.
Done 400 examples.
Done 500 examples.
Done 600 examples.
Done 700 examples.
Done 800 examples.
Done 900 examples.
Done 1000 examples.
Done 1100 examples.
Done 1200 examples.
Done 1300 examples.
Done 1400 examples.
Done 1500 examples.
Done 1600 examples.
Done 1700 examples.
Done 1800 examples.
Done 1900 examples.
Done 2000 examples.
Done 2100 examples.
Done 2200 examples.
Done 2300 examples.
Done 2400 examples.
Done 2500 examples.
Done 2600 examples.
Done 2700 examples.
Done 2800 examples.
Mapping of classes to integers (integer class, original class): [(0, 'THANK'), (1, 'DIRECT'), (2, 'COMPLAIN'), (3, 'ASSERT')]
Distribution of classes: {'DIRECT': 206, 'COMPLAIN': 368, 'ASSERT': 1181, 'THANK': 78}
Dataset shape: ((1833, 9751), (1833,))
FSS 'Select from model'
1489
Dataset shape: ((1489, 595), (1489,), (344, 595), (344,))
	P_weighted: 0.76, R_weighted: 0.68, F1_weighted: 0.71
SWDA Eval
preetify
adding tokens
adding lemmas
Train examples #: 1413 , test examples #: 4451
Doing feature group: [1, 2, 3]
NGram: created  2342 ngram features.
NGram: created  5021 ngram features.
NGram: created  2757 ngram features.
Done 0 examples.
Done 100 examples.
Done 200 examples.
Done 300 examples.
Done 400 examples.
Done 500 examples.
Done 600 examples.
Done 700 examples.
Done 800 examples.
Done 900 examples.
Done 1000 examples.
Done 1100 examples.
Done 1200 examples.
Done 1300 examples.
Done 1400 examples.
Done 1500 examples.
Done 1600 examples.
Done 1700 examples.
Done 1800 examples.
Done 1900 examples.
Done 2000 examples.
Done 2100 examples.
Done 2200 examples.
Done 2300 examples.
Done 2400 examples.
Done 2500 examples.
Done 2600 examples.
Done 2700 examples.
Done 2800 examples.
Done 2900 examples.
Done 3000 examples.
Done 3100 examples.
Done 3200 examples.
Done 3300 examples.
Done 3400 examples.
Done 3500 examples.
Done 3600 examples.
Done 3700 examples.
Done 3800 examples.
Done 3900 examples.
Done 4000 examples.
Done 4100 examples.
Done 4200 examples.
Done 4300 examples.
Done 4400 examples.
Done 4500 examples.
Done 4600 examples.
Done 4700 examples.
Done 4800 examples.
Done 4900 examples.
Done 5000 examples.
Done 5100 examples.
Done 5200 examples.
Done 5300 examples.
Done 5400 examples.
Done 5500 examples.
Done 5600 examples.
Done 5700 examples.
Done 5800 examples.
Done 5900 examples.
Done 6000 examples.
Done 6100 examples.
Done 6200 examples.
Done 6300 examples.
Done 6400 examples.
Done 6500 examples.
Done 6600 examples.
Done 6700 examples.
Done 6800 examples.
Done 6900 examples.
Done 7000 examples.
Done 7100 examples.
Done 7200 examples.
Done 7300 examples.
Done 7400 examples.
Done 7500 examples.
Done 7600 examples.
Done 7700 examples.
Done 7800 examples.
Done 7900 examples.
Done 8000 examples.
Done 8100 examples.
Done 8200 examples.
Done 8300 examples.
Done 8400 examples.
Done 8500 examples.
Done 8600 examples.
Done 8700 examples.
Done 8800 examples.
Done 8900 examples.
Done 9000 examples.
Done 9100 examples.
Done 9200 examples.
Done 9300 examples.
Done 9400 examples.
Done 9500 examples.
Done 9600 examples.
Done 9700 examples.
Done 9800 examples.
Done 9900 examples.
Done 10000 examples.
Done 10100 examples.
Done 10200 examples.
Done 10300 examples.
Done 10400 examples.
Done 10500 examples.
Done 10600 examples.
Done 10700 examples.
Done 10800 examples.
Done 10900 examples.
Done 11000 examples.
Done 11100 examples.
Done 11200 examples.
Done 11300 examples.
Done 11400 examples.
Done 11500 examples.
Done 11600 examples.
Done 11700 examples.
Done 11800 examples.
Done 11900 examples.
Done 12000 examples.
Done 12100 examples.
Done 12200 examples.
Done 12300 examples.
Mapping of classes to integers (integer class, original class): [(0, 'THANK'), (1, 'GUESS'), (2, 'ENGAGE'), (3, 'DISAGREE'), (4, 'DIRECT'), (5, 'ASSERT'), (6, 'AGREE')]
Distribution of classes: {'DIRECT': 563, 'DISAGREE': 78, 'ENGAGE': 73, 'ASSERT': 4427, 'THANK': 54, 'GUESS': 171, 'AGREE': 498}
Dataset shape: ((5864, 13032), (5864,))
FSS 'Select from model'
1413
Dataset shape: ((1413, 909), (1413,), (4451, 909), (4451,))
	P_weighted: 0.85, R_weighted: 0.82, F1_weighted: 0.83
