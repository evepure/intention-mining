\begin{table*}[h!tb]
	\centering
    \caption{Multi-class prediction results on {\it Bhatia} and {\it SWDA}--10K samples only. Feature groups \{1,2,3\} are Discourse, Content and Conversation. Best results are in bold.}
	\begin{tabular}{cccccccc}\hline
    	\bf Feature group & \{1\} & \{2\} & \{3\} & \{1,2\} & \{2,3\} & \{1,3\} & \{1,2,3\} \\\hline
    	  
      \bf Bhatia & 0.67 & \bf 0.75 & 0.66 & 0.72 & 0.73 & 0.67 & 0.71 \\  
      \bf SWDA & 0.76 & 0.82 & 0.70 & \bf 0.84 & 0.80 & 0.72 & 0.83 \\ \hline
      
        \end{tabular}
    \label{tab:bhatiaSWDAResults}
\end{table*}