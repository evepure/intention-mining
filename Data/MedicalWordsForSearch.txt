abdomen
stomach
tummy
belly
abrasion
scrape
scratch
agitation
anxiety
restlessness
nervousness
ailment
sickness
illness
health problem
allergen
allergy
allergic rhinitis
hay fever
alopecia
hair loss
amelioration
analgesic
pain killer
pain reliever
anaphylaxis
allergic reaction
anaesthetic
angina
chest pain
anticoagulant
blood thinner
anti-inflammatory
arrhythmia
arteriosclerosis
hardening of the arteries
ascorbic acid
vitamin C
asphyxiation
choking
suffocation
aspiration
assay
lab test
asymptomatic
without symptoms
atopic dermatitis
itchy red rash
bacteria
germs
benign
biopsy
blood glucose
blood sugar
blood profile
body mass index
bradycardia
slow heart beat
buttocks
butt
carcinogen
carcinoma
cancer
cardiac
cardiologist
heart doctor
carpal
wrist
catheter
cell culture
cellulitis
skin infection
central nervous system
brain cord
spine cord
cerebral haemorrhage
cerebral accident
cerebrovascular accident
stroke
blood clot in the brain
chemotherapy
chest film
chest xray
cholesterol
chronic
clavicle
collarbone
clinical
medical care
clinical trial
coagulation
colon
colorectal
large intestine
colonoscopy
computem tomography
condition
health problem
congenital
congenital anomaly
birth defect
congestive heart failure
conjunctivitis
pink eye
contraceptive
contraindicated
controlled trial
contusion
bruise
convulsion
seizure
shaking
coronary
blood vessels
coronary thrombosis
cutaneous
skin
debilitating
deep vein thrombosis
deficiency
degeneration
diabetic
diagnosis
diagnostic procedure
diaphoresis
diuretic
diverticulitis
dysfunction
dysmenorrhea
painful period cramps
dyspepsia
heartburn
dysphagia
trouble swallowing
dyspnea
trouble breathing
echocardiography
echocardiogram
edema
swelling
electrolytes
embolism
lump of blood
clot
embolus
emesis
throwing up
vomiting
endometrium
lining of the uterus
enuresis
problems controlling urine
bladder control problems
epidemiologist
aetiology
cause
eruption
rash
breakout
excise
febrile
fever
femur
thigh bone
fetus
fracture
gastric
stomach
gastroenterologist
digestion
gastroesophageal reflux
gerontological
gestation
pregnancy
gynecologist
hearing impairment
hearing loss
dearness
heart failure
hematocrit
red blood cells
hematoma
bruise
haemorrhage
heavy bleeding
hepatic
liver
heritable
hereditary
genetic
herpes
cold sore
shingles
hirsutism
hyperopia
farsighted
hypersensitivity
hypertension
hypotension
blood pressure
hyperthyroidism
hypothyroidism
overactive thyroid
thyroid hormone
hypoxia
idiopathic
immunotherapy
autoimmune
implant
impotent
impotence
in vitro
incision
infectious
infection
illness
sickness
disease
infertile
inflammation
inhibitor
injection
shot
internist
intervention
treatment
intramuscular
intravenous
intubation
invasive procedure
investigation
investigator
jaundice
laceration
lactation
lactose
larynx
limb
arm
leg
lingual
tongue
lipids
fats in blood
lipid profile
lymphoma
lymph nodes
magnetic resonance
malaise
feeling sick
feeling bad
malignant
malignancy
malingering
medication
drug
medicine
menarche
period
menopause
menses
menstruation
metabolise
metastatic
miliaria
prickly heat
morbidity
disease rate
illness rate
mortality
death rate
musculoskeletal
mutation
genetic defect
myocardial infarction
heart attack
myopia
nausea
upset stomach
nephropathy
kidney disease
neuralgia
nerve pain
neuron
nerve cell
nodule
lump
noninvasive
nutrient
oncologist
oncology
orthopaedic
bones
osteoporosis
bone disease
otolaryngologist
palliative
pallor
paleness
palpitation
fast heartbeat
paraesthaesia
parturition
labor
delivery
childbirth
patogen
pharmacotherapy
pharyngitis
sore throat
physician
doctor
placebo
placenta
plaque
artery
plasma
plasma glucose
pet scan
positron emission tomography
postoperative
preclinical
prenatal
prophylaxis
prosthesis
psychopathology
psychosocial
mental illness
psychotropic
pulmonary
lungs
radiologist
radiology
reflux
regimen
remission
renal
retinol
rupture
sarcoma
sclerosis
sedative
sedentary
sepsis
sinusitis
sinus infection
somnolence
sleepiness
strep
streptococcal
subcutaneous
sublingual
sutures
stitches
symptomatic
having symptoms
systemic
tachycardia
tear ligament
torn ligament
sprain
phone
terminal
not curable
therapeutic modality
thoracic
chest
thrombosis
torso
toxic
toxin
toxicity
trachea
windpipe
transdermal
transmission
trauma
injury
tremor
urine test
urinalysis
varicella
chickenpox
vertigo
dizziness
vitals
vital signs