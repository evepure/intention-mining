## Intention mining

This repository accompanies the paper *[Process Models of Interrelated Speech Intentions from Online Health-related Conversations](https://en.wikipedia.org/wiki/Todo)* published in *Artificial Intelligence In Medicine*.

### Folder structure

The project consists of the following folders:

* *Data*: Supporting datasets and dependencies.
* *EvaluationResults*: Logs of running different configurations of analyses.
* *PythonProject*: Python project.

### Project setup

1. Anaconda 3 environment creation (in folder *PythonProject*).

		conda env create -f environment.yml
		conda activate intention-mining

2. NLTK dependencies installation (in the Python environment shell).

		import nltk
		nltk.download("words")
		nltk.download("punkt")
		nltk.download("wordnet")

3. Creation of the file *PythonProject/constants.py* based on template file *PythonProject/constants_TEMPLATE.py*.


### Analyses running

1. Open PyCharm IDE (or other if you prefer) and create new project in folder *PythonProject*.
2. Analyses that can generate results, published in the paper:
	* *standardClassificationRunner.py*: Multi-class classification settings.
	* *oneVsAllClassificationRunner.py*: Manually mapping of classes into one-vs-all settings separately.
	* *multilabelClassificationRunner.py*: Multi-label classification settings.
	* *trainTestClassificationRunner.py*: SWDA and Bhatia datasets training and prediction on their parts of pre-separated datasets.
	* *exploratoryAnalysis.py*: Output of basic statistics for a seleced dataset (#tokens, #sentences, #posts, ...).
